﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Text;
using DataAccessLayer;

namespace BusinessLogicLayer
{
   public class PersonalDetail
    {
       public String IDNo { get; set; }
       public String FName { get; set; }
       public String LName{ get; set; }
       public String CellNo { get; set; }
       public String AltCellNo{ get; set; }
       public String HomeAdd { get; set; }
       public String Race { get; set; }
       public String Email { get; set; }
       public String Gender { get; set; }
       public String Lang { get; set; }
       public String Nation { get; set; }
       public String Disability { get; set; }
       public String Qulif {get;set;}
       public String Inst {get;set;}
       public string Projname { get; set; }

   //    public PersonalDetail()
   //{

   //    IDNo = String.Empty;
   //    FName = String.Empty;
   //    LName = String.Empty;
   //    CellNo = String.Empty;
   //    AltCellNo = String.Empty;
   //    Email = String.Empty;
   //    Gender = String.Empty;
   //    HomeAdd = String.Empty;
   //    Race = String.Empty;
   //    Lang = String.Empty;
   //    Nation = String.Empty;
   //    Disability = String.Empty;
   //        Qulif=String.Empty;
   //        Inst=String.Empty;
   //        Projname = String.Empty;
             
   //}


       public bool Save_Details(string idno, string Fname, string Lname, string cellno, string email, string altname, string homeadd, string race,string gender, string lang, string nation, string disa,string proj)
       {

           csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();
           objist.Add(new csParameterListType("ID_Num", System.Data.SqlDbType.VarChar, idno));
           objist.Add(new csParameterListType("FName", System.Data.SqlDbType.VarChar,Fname));
           objist.Add(new csParameterListType("LName", System.Data.SqlDbType.VarChar, Lname));
           objist.Add(new csParameterListType("CellNum", System.Data.SqlDbType.BigInt, cellno));
           objist.Add(new csParameterListType("Email", System.Data.SqlDbType.VarChar, email));
           objist.Add(new csParameterListType("Alt_CellNum", System.Data.SqlDbType.BigInt, altname));
           objist.Add(new csParameterListType("HomeAdd", System.Data.SqlDbType.VarChar, homeadd));
           objist.Add(new csParameterListType("Race", System.Data.SqlDbType.VarChar, race));
           objist.Add(new csParameterListType("Gender", System.Data.SqlDbType.VarChar, gender));
           objist.Add(new csParameterListType("Lang", System.Data.SqlDbType.VarChar, lang));
           objist.Add(new csParameterListType("Nation", System.Data.SqlDbType.VarChar, nation));
           objist.Add(new csParameterListType("Disability", System.Data.SqlDbType.VarChar, disa));
           objist.Add(new csParameterListType("Projname", System.Data.SqlDbType.VarChar, proj));
           if (dal.executespreturnnd("SaveP_Details", objist))
               return true;
           else return false;
       
       }
    
       public bool savequalification(string idno, string progname, string qulification, string inst)
       {
           csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();
           objist.Add(new csParameterListType("ID_Num", System.Data.SqlDbType.VarChar, idno));
           objist.Add(new csParameterListType("projname", System.Data.SqlDbType.VarChar, progname));
           objist.Add(new csParameterListType("qulif", System.Data.SqlDbType.VarChar, qulification));
           objist.Add(new csParameterListType("inst", System.Data.SqlDbType.VarChar, inst));

           if (dal.executespreturnnd("SaveQ_Details", objist))
               return true;
           else return false;
       }

       public bool SaveInterEntry(string idno, string compname, string unitstd)
       {
           csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();
           //objist.Add(new csParameterListType("CompRId", System.Data.SqlDbType.VarChar, compid));
           objist.Add(new csParameterListType("Compname", System.Data.SqlDbType.VarChar, compname));
           objist.Add(new csParameterListType("idnum", System.Data.SqlDbType.VarChar, idno));
           objist.Add(new csParameterListType("ustd", System.Data.SqlDbType.VarChar, unitstd));

           if (dal.executespreturnnd("InterEntry_SP", objist))
               return true;
           else return false;
       }

       public string Get_ID_Num(string idno)
       {
           csDAL dal = new csDAL();
           List<csParameterListType> objPar = new List<csParameterListType>();

           objPar.Add(new csParameterListType("ID_Num", System.Data.SqlDbType.VarChar, idno));

           PersonalDetail per = new PersonalDetail();

           string id = "";
           using (IDataReader dr = dal.executespreturndr("searchIDNo", objPar))
           {

               while (dr.Read())
               {

                   if (!dr.IsDBNull(0))
                   {

                       per.IDNo = dr.GetString(0);
                       id= per.IDNo;
                     
                    
                   }




               }
           }

           return id;

       }
           
       public bool GetUserdetail(string idno)
        {
            csDAL dal = new csDAL();
            List<csParameterListType> objPar = new List<csParameterListType>();

            objPar.Add(new csParameterListType("ID_Num", System.Data.SqlDbType.VarChar, idno));

           PersonalDetail pers=new PersonalDetail ();

           try
           {
               using (IDataReader dr = dal.executespreturndr("ShowStud_Det", objPar))
               {

                   while (dr.Read())
                   {

                       IDNo = dr["ID_Num"].ToString();

                       FName = dr["FName"].ToString();
                       LName = dr["LName"].ToString();
                       CellNo = dr["CellNum"].ToString();
                       AltCellNo = dr["Alt_CellNum"].ToString();
                       Email = dr["Email"].ToString();
                       Gender = dr["Gender"].ToString();
                       HomeAdd = dr["HomeAdd"].ToString();
                       Race = dr["Race"].ToString();
                       Lang = dr["Lang"].ToString();
                       Nation = dr["Nation"].ToString();
                       Disability = dr["Disability"].ToString();
                       Qulif = dr["Qualif"].ToString();
                       Inst = dr["Institution"].ToString();
                       Projname = dr["ProjName"].ToString();
                       return true;
                   }
                   return false;
               }
           }
           catch (Exception ec)
           {
               return false;
           }
       }


    }
}
