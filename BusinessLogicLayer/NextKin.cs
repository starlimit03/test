﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using System.Data.SqlClient;

namespace BusinessLogicLayer
{
    public class NextKin
    {
        public string ID_Num { get; set; }

        public string Name { get; set; }
        public string Relationship { get; set; }
        public string ContactNum { get; set; }
        public string AltCont_Num { get; set; }
        public string Gender { get; set; }
        public string HomeAdd { get; set; }
        public string Tsize { get; set; }



        public bool SaveNextOfKin(string idno, string name, string relation, string contnum, string altnum, string gender, string homeadd, string tsize)
        {
            csDAL dal = new csDAL();
            List<csParameterListType> objList = new List<csParameterListType>();
            objList.Add(new csParameterListType("ID_Num", SqlDbType.VarChar, idno));
            objList.Add(new csParameterListType("Relationship", SqlDbType.VarChar, relation));
            objList.Add(new csParameterListType("Name", SqlDbType.VarChar, name));
            objList.Add(new csParameterListType("ContactNum", SqlDbType.VarChar, contnum));
            objList.Add(new csParameterListType("AltCont_Num", SqlDbType.VarChar,  altnum));
            objList.Add(new csParameterListType("Gender", SqlDbType.VarChar, gender));
            objList.Add(new csParameterListType("HomeAdd", SqlDbType.VarChar, homeadd));
            objList.Add(new csParameterListType("Tsize", SqlDbType.VarChar, tsize));
            if (dal.executespreturnnd("SaveN_Details", objList))
                return true;
            else
                return false;          
        
        
        
        }




    }

}
