﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;

namespace BusinessLogicLayer
{
  public  class Lists
    {


        public DataSet GetInterList(string idno, string group, string listtype, string type)
        {

            csDAL dal = new csDAL();
            List<csParameterListType> objist = new List<csParameterListType>();
            objist.Add(new csParameterListType("ID", System.Data.SqlDbType.VarChar, idno));
            objist.Add(new csParameterListType("Grp", System.Data.SqlDbType.VarChar, group));
            objist.Add(new csParameterListType("listtype", System.Data.SqlDbType.VarChar, listtype));
            objist.Add(new csParameterListType("type", System.Data.SqlDbType.VarChar, type));

            DataSet dr = null;
            
            using ( dr= dal.executespreturnds("Search_InterviewList_SP", objist))
            {
                if (dr.Tables[0].Rows.Count >0 ) return dr;
                else
                    return null;
            }

        }

        public bool UpdateList(string id, string date, string time,string venue,string projname)
        {

            csDAL dal = new csDAL();
            List<csParameterListType> objist = new List<csParameterListType>();
            objist.Add(new csParameterListType("ID", System.Data.SqlDbType.VarChar, id));
            objist.Add(new csParameterListType("Date", System.Data.SqlDbType.VarChar, date));
            objist.Add(new csParameterListType("DTime", System.Data.SqlDbType.VarChar, time));
            objist.Add(new csParameterListType("Venue", System.Data.SqlDbType.VarChar, venue));
            objist.Add(new csParameterListType("prjname", System.Data.SqlDbType.VarChar, projname));


            if (dal.executespreturnnd("UpdateList_SP", objist))
                return true;
            else return false;
        }

        public DataSet GetScheduledList(string frm,string to,string grp,string ven,string type)
        {

            csDAL dal = new csDAL();
            List<csParameterListType> objist = new List<csParameterListType>();
            objist.Add(new csParameterListType("calfrm", System.Data.SqlDbType.VarChar, frm));
            objist.Add(new csParameterListType("calto", System.Data.SqlDbType.VarChar, to));
            objist.Add(new csParameterListType("grp", System.Data.SqlDbType.VarChar, grp));
            objist.Add(new csParameterListType("venue", System.Data.SqlDbType.VarChar, ven));
            objist.Add(new csParameterListType("type", System.Data.SqlDbType.VarChar, type));
            
            DataSet dr = null;

            using (dr = dal.executespreturnds("GetInterviewedList_SP", objist))
            {
                if (dr != null)
                { if (dr.Tables[0].Rows.Count > 0) return dr; }
                else
                    return null;
            }
            return null;
        }

        public bool UpdateInterviewList(string id, int scor, string outcom, string rsn, string recommend,string approv,string typ)
        {

            csDAL dal = new csDAL();
            List<csParameterListType> objist = new List<csParameterListType>();
            objist.Add(new csParameterListType("ID", System.Data.SqlDbType.VarChar, id));
            objist.Add(new csParameterListType("score", System.Data.SqlDbType.Int, scor));
            objist.Add(new csParameterListType("outcome", System.Data.SqlDbType.VarChar, outcom));
            objist.Add(new csParameterListType("reason", System.Data.SqlDbType.VarChar, rsn));
            objist.Add(new csParameterListType("recom", System.Data.SqlDbType.VarChar, recommend));
            objist.Add(new csParameterListType("approve", System.Data.SqlDbType.VarChar, approv));
            objist.Add(new csParameterListType("type", System.Data.SqlDbType.VarChar, typ));


            if (dal.executespreturnnd("UpdateInterviewOutcome_SP", objist))
                return true;
            else return false;
        }
    }
}
