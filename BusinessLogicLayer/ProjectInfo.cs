﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using System.Data;

namespace BusinessLogicLayer
{
    public class ProjectInfo
    {
        public int Progid { get; set; }
        public string ProgName { get; set; }
        public string Desc { get; set; }
        public int Benef { get; set; }
        public string Elig { get; set; }
        public int SumBenef { get; set; }


    
        public bool CreateProject(string name,string prognam, string desc, string sdate,string edate,
            string sponname,string spongrp, 
            int pben, string elig, string unitstd, int crdt,string nqf, 
            string stip, double stiamt, string stifreq,string phas,string deduct,double deduction)
        {
            try
            {    csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();
                objPar.Add(new csParameterListType("pname", SqlDbType.VarChar, name));
                objPar.Add(new csParameterListType("pgrmname", SqlDbType.VarChar, prognam));
                objPar.Add(new csParameterListType("pdesc", SqlDbType.VarChar, desc));
                objPar.Add(new csParameterListType("sdate", SqlDbType.VarChar, sdate));
                objPar.Add(new csParameterListType("edate", SqlDbType.VarChar, edate));
                objPar.Add(new csParameterListType("sponsname", SqlDbType.VarChar, sponname));
                objPar.Add(new csParameterListType("sponsgrp", SqlDbType.VarChar, spongrp));
                objPar.Add(new csParameterListType("pbenef", SqlDbType.Int, pben));
                objPar.Add(new csParameterListType("elig", SqlDbType.VarChar, elig));
                objPar.Add(new csParameterListType("unitstd", SqlDbType.VarChar, unitstd));
                objPar.Add(new csParameterListType("pcrdt", SqlDbType.Int, crdt));
                objPar.Add(new csParameterListType("nqflvl", SqlDbType.VarChar, nqf));
                objPar.Add(new csParameterListType("stipend", SqlDbType.VarChar, stip));
                objPar.Add(new csParameterListType("stipendamt", SqlDbType.Float, stiamt));
                objPar.Add(new csParameterListType("stipendfreq", SqlDbType.VarChar, stifreq));
                objPar.Add(new csParameterListType("phase", SqlDbType.VarChar, phas));
                objPar.Add(new csParameterListType("Deduct", SqlDbType.VarChar, deduct));
                objPar.Add(new csParameterListType("Deductamt", SqlDbType.Float, deduction));
              
                if (objDAL.executespreturnnd("CreateProject_SP", objPar))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;

            }

        }


        public bool CreateSite(string pname, string sname, int pbenef, int sbenef, string sdate, string edate,
        string province, string region, string town,
       string p_mgr, string t_mgr, string p_admin,string p_fac)
        {
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();
                objPar.Add(new csParameterListType("prjname", SqlDbType.VarChar, pname));
                objPar.Add(new csParameterListType("sitename", SqlDbType.VarChar, sname));
                objPar.Add(new csParameterListType("pbenef", SqlDbType.Int, pbenef));
                objPar.Add(new csParameterListType("sbenef", SqlDbType.Int, sbenef));
                objPar.Add(new csParameterListType("sdate", SqlDbType.VarChar, sdate));
                objPar.Add(new csParameterListType("edate", SqlDbType.VarChar, edate));
                objPar.Add(new csParameterListType("province", SqlDbType.VarChar, province));
                objPar.Add(new csParameterListType("region", SqlDbType.VarChar, region));
                objPar.Add(new csParameterListType("town", SqlDbType.VarChar, town));
                objPar.Add(new csParameterListType("p_mgr", SqlDbType.VarChar, p_mgr));
                objPar.Add(new csParameterListType("p_admin", SqlDbType.VarChar, p_admin));
                objPar.Add(new csParameterListType("t_mgr", SqlDbType.VarChar, t_mgr));
                objPar.Add(new csParameterListType("p_fac", SqlDbType.VarChar, p_fac));

                if (objDAL.executespreturnnd("CreateSite_SP", objPar))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;

            }

        }

        public string GetProjectEligibility(string pname)
        {
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();

                // remove ................Pick from xml
                objPar.Add(new csParameterListType("projname", SqlDbType.VarChar, pname));

                using (IDataReader dr = objDAL.executespreturndr("GetProjectElig_SP", objPar))
                {
                    if (dr.Read())
                    {
                        Elig = dr.GetValue(0).ToString();
                    }
                    return Elig;
                }
            }
            catch (Exception ex)
            {
                return null;

            }

        }
        public bool GetProjectInfo(string name)
        {
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();

                objPar.Add(new csParameterListType("pname", SqlDbType.VarChar, name));

                using (IDataReader dr = objDAL.executespreturndr("GetProjectInfo_SP", objPar))
                {
                    if (dr.Read())
                    {
                        ProgName = dr.GetValue(1).ToString();
                        Desc = dr.GetValue(2).ToString();
                        Benef = Convert.ToInt16(dr.GetValue(3));
                    }
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;

            }

        }

        public bool GetBeneficiary(string name)
        {
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();

                // remove ................Pick from xml
                objPar.Add(new csParameterListType("progname", SqlDbType.VarChar, name));

                using (IDataReader dr = objDAL.executespreturndr("GetBeneficiary_SP", objPar))
                {
                    if (dr.Read())
                    {
                        Benef = Convert.ToInt16(dr.GetValue(0));
                        SumBenef = Convert.ToInt16(dr.GetValue(1));
                        return true;
                    }
                    else
                    {
                        Benef = 0; SumBenef = 0; return false;
                    }
                   
                }
              
            }
            catch (Exception ex)
            {
                return false;
                
            }

        }
        public DataSet GetAllProject(string prog,string type)
        {

            try
            {
                csDAL objDAL = new csDAL();
                DataSet ds = new DataSet();
                List<csParameterListType> objPar = new List<csParameterListType>();
                // remove ................Pick from xml
                objPar.Add(new csParameterListType("pname", SqlDbType.VarChar, prog));
                objPar.Add(new csParameterListType("type", SqlDbType.VarChar, type));

                ds = objDAL.executespreturnds("GetAllProject_SP", objPar);               
                if (ds != null)
                    return ds;
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;

            }

        }
    }
}
