﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using System.Data;

namespace BusinessLogicLayer
{
  public class RegisterList
    {

      public DataSet GetRegister(string grp, string com, string typ, string RegID, int wk, int days, string id)
      {
          csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();
           objist.Add(new csParameterListType("Grp", System.Data.SqlDbType.VarChar, grp));
           objist.Add(new csParameterListType("type", System.Data.SqlDbType.VarChar, typ));
           objist.Add(new csParameterListType("Regid", System.Data.SqlDbType.VarChar, RegID));
           objist.Add(new csParameterListType("week", System.Data.SqlDbType.Int, wk));
           objist.Add(new csParameterListType("day", System.Data.SqlDbType.Int, days));
           objist.Add(new csParameterListType("comment", System.Data.SqlDbType.VarChar, com));
           objist.Add(new csParameterListType("idnum", System.Data.SqlDbType.VarChar, id));
         
          DataSet dr = null;

          using (dr = dal.executespreturnds("GetRegister_SP", objist))
          {
              if(dr!=null)
                if (dr.Tables[0].Rows.Count > 0) return dr;
            
              return null;
          }
          //return null;
      }

      public bool UpdateRegister(string grp, string com, string typ, string RegID, int wk, int days, string id)
      {
          csDAL dal = new csDAL();
          List<csParameterListType> objist = new List<csParameterListType>();
          objist.Add(new csParameterListType("Grp", System.Data.SqlDbType.VarChar, grp));
          objist.Add(new csParameterListType("type", System.Data.SqlDbType.VarChar, typ));
          objist.Add(new csParameterListType("Regid", System.Data.SqlDbType.VarChar, RegID));
          objist.Add(new csParameterListType("week", System.Data.SqlDbType.Int, wk));
          objist.Add(new csParameterListType("day", System.Data.SqlDbType.Int, days));
          objist.Add(new csParameterListType("comment", System.Data.SqlDbType.VarChar, com));
          objist.Add(new csParameterListType("idnum", System.Data.SqlDbType.VarChar, id));

       //  DataSet dr = null;

          if(dal.executespreturnnd("CreateRegister_SP", objist))
          
              return true;
          else    return false;
         
          //return null;
      }

   

   }
}
