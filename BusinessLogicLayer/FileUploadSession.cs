﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;

namespace BusinessLogicLayer
{
   public class FileUploadSession
    {
        Session[] DBdata = new Session[6];



        public bool SaveInfo(string name, string id, string contentType, int size, string pasth)
        {
            csDAL objDAL = new csDAL();
            List<csParameterListType> objPar = new List<csParameterListType>();
            objPar.Add(new csParameterListType("@Name", SqlDbType.NVarChar, name));
            objPar.Add(new csParameterListType("@IDNo", SqlDbType.VarChar, id));
            objPar.Add(new csParameterListType("@ContentType", SqlDbType.NVarChar, contentType));
            objPar.Add(new csParameterListType("@Size", SqlDbType.Int, size));
            // objPar.Add(new csParameterListType("@Data", SqlDbType.VarBinary, data));
            objPar.Add(new csParameterListType("@Path", SqlDbType.VarChar, pasth));




            objDAL.executespreturnnd("savefileupload", objPar);
            return true;


        }
        public bool UpdateInfo(int sn, string name, string id, string contentType, int size, string path)
        {
            csDAL objDAL = new csDAL();
            List<csParameterListType> objPar = new List<csParameterListType>();


            objPar.Add(new csParameterListType("@Doc_ID", SqlDbType.Int, sn));
            objPar.Add(new csParameterListType("@Name", SqlDbType.NVarChar, name));
            objPar.Add(new csParameterListType("@IDNo", SqlDbType.VarChar, id));

            objPar.Add(new csParameterListType("@ContentType", SqlDbType.NVarChar, contentType));
            objPar.Add(new csParameterListType("@Size", SqlDbType.Int, size));
            objPar.Add(new csParameterListType("@Path", SqlDbType.VarChar, path));



            objDAL.executespreturnnd("UpdatefileUpload", objPar);
            return true;
        }

        public Session[] GetInfo(string SN)
        {
            
            //Session [] dataarray=new Session[4];

            csDAL objDAL = new csDAL();
            List<csParameterListType> objPar = new List<csParameterListType>();

            objPar.Add(new csParameterListType("ID", System.Data.SqlDbType.VarChar, SN));

            using (IDataReader dr = objDAL.executespreturndr("get_file", objPar))
            {
                // SqlDataReader drd = new SqlDataReader();
                //do
                //{

                int i = 0; bool hasData = false;
                while (dr.Read())
                {

                    //for (int i = 0; i < dr.FieldCount; i++)
                    //{
                    hasData = true;

                    DBdata[i] = new Session();
                    DBdata[i].SN = dr.GetInt32(0);
                    DBdata[i].SName = dr.GetString(1);
                    DBdata[i].SSize = dr.GetInt32(4);
                    DBdata[i].SType = dr.GetString(3);
                    DBdata[i].SPath = dr.GetString(5);
                    // byte[] byteArray=(Byte[])dr["Data"];
                    //DBdata[i].SData = byteArray;

                    // return DBdata;

                    i++;
                    // dr.NextResult();
                    //}
                }

                if (hasData)
                    return DBdata;
                else return null;

            }
        }
    }
}
