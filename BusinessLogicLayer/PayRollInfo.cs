﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;

namespace BusinessLogicLayer
{
   public class PayRollInfo
   {  
     public DataSet GetPayroll(string PayID, string RegID)
      {
          csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();

           objist.Add(new csParameterListType("Regid", System.Data.SqlDbType.VarChar, RegID));
           objist.Add(new csParameterListType("Payid", System.Data.SqlDbType.VarChar, PayID));
         
          DataSet dr = null;

          using (dr = dal.executespreturnds("GetPayroll_SP", objist))
          {
              if(dr!=null)
                if (dr.Tables[0].Rows.Count > 0) return dr;
            
              return null;
          }
      }
   
       public bool UpdatePayroll(string id, string RegID, string PayID, string stat)
         {
             csDAL dal = new csDAL();
             List<csParameterListType> objist = new List<csParameterListType>();

             objist.Add(new csParameterListType("regid", System.Data.SqlDbType.VarChar, RegID));
             objist.Add(new csParameterListType("payid", System.Data.SqlDbType.VarChar, PayID));
             objist.Add(new csParameterListType("stat", System.Data.SqlDbType.VarChar, stat));
             objist.Add(new csParameterListType("id", System.Data.SqlDbType.VarChar, id));

             if (dal.executespreturnnd("UpdatePayroll_SP", objist))
                 return true;
             else
                 return false;
         }

       public DataSet GetRegisterStat(string RegID)
       {
           csDAL dal = new csDAL();
           List<csParameterListType> objist = new List<csParameterListType>();
           objist.Add(new csParameterListType("Regid", System.Data.SqlDbType.VarChar, RegID));

           DataSet dr = null;

           using (dr = dal.executespreturnds("GetRegisterStat_SP", objist))  // Count of Beneficiaries, Sum of total payments and the sum of decuctions
           {
               if (dr != null)
                   if (dr.Tables[0].Rows.Count > 0) return dr;

               return null;
           }
           //return null;
       }
    }
}
