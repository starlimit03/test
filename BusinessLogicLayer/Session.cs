﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer
{
    public class Session
    {
        public int SN { get; set; }
        public int SSize { get; set; }
        public string SName { get; set; }
        public string SType { get; set; }
        public string FieldName { get; set; }
        public byte[] SData { get; set; }
        public string SPath { get; set; }
  
    }
}
