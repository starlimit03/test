﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.LearnersInfo
{
    class PersonalDetails
    {
        public int IdNo { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public Int32 CellNo { get; set; }
        public Int32 AltCellNo { get; set; }
        public string  Address { get; set; }
        public string Race { get; set; }
        public string Lang { get; set; }
        public string Nation { get; set; }
        public string Disable { get; private set; }

      
        
    }
}
