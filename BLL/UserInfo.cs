﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class UserInfo
    {
        public string Fname { get; set; }
        public string Lname { get; set; }
        public int EmpId { get; set; }
        public static string SessionId { get; set; } // to prevent having to call on each page
    }
}
