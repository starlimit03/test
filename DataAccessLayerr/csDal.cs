﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccessLayerr
{
    public class csDAL
    {
        protected string _strconn = System.Configuration.ConfigurationManager.ConnectionStrings["testingConnectionString"].ToString();
       
        private SqlConnection conn;
        private string errorstr = string.Empty;

        public csDAL()
        {
            conn = new SqlConnection();
        }

        public string ConnectionString
        {
            get { return _strconn; }
            set { _strconn = value; }
        }

        private bool Open_Connection()
        {
            if (conn == null)
                conn = new SqlConnection();

            if (conn.State == System.Data.ConnectionState.Closed)
            {
                try
                {
                    conn.ConnectionString = ConnectionString;
                    conn.Open();
                    return true;
                }
                catch (Exception a)
                {
                    errorstr += " " + a.Message;
                    return false;
                }
            }
            else
                return true;
        }

        public IDataReader executespreturndr(string spname)
        {
            SqlCommand cmd = new SqlCommand();
            IDataReader dr = null;

            try
            {
                if (Open_Connection())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = spname;
                    dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                return dr;
            }
            catch (Exception e)
            {
                errorstr += " " + e.Message;
                return null;
            }
        }

        public IDataReader executespreturndr(string spname, List<csParameterListType> objlist)
        {
            SqlCommand cmd = new SqlCommand();
            IDataReader dr = null;

            try
            {
                if (Open_Connection())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = spname;

                    foreach (csParameterListType pv in objlist)
                    {
                        cmd.Parameters.Add(add_parameters(pv));
                    }

                    dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                return dr;
            }
            catch (Exception e)
            {
                errorstr += " " + e.Message;
                return null;
            }
        }

        public System.Data.DataSet executespreturnds(string spname)
        {
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter sda = new SqlDataAdapter();
            DataSet ds = new DataSet();

            if (Open_Connection())
            {
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = spname;

                sda.SelectCommand = cmd;

                sda.Fill(ds);
            }

            conn.Close();
            return ds;
        }

        public int executespreturnds(String spname, List<csParameterListType> objlist)
        {
            SqlCommand cmd = new SqlCommand();
            if (Open_Connection())
            {
                cmd.Connection = conn;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = spname;

                foreach (csParameterListType pv in objlist)
                {
                    cmd.Parameters.Add(add_parameters(pv));
                }
            }
            return (cmd.ExecuteNonQuery());
          
        }

        private SqlParameter add_parameters(csParameterListType objpar)
        {
            SqlParameter sqlpar = new SqlParameter();
            sqlpar.ParameterName = objpar.Name;
            sqlpar.SqlDbType = objpar.SqlType;
            sqlpar.SqlValue = objpar.Value;

            return sqlpar;
        }

        public void executespreturnnd(string spname, List<csParameterListType> objlist)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (Open_Connection())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = spname;

                    foreach (csParameterListType par in objlist)
                    {
                        cmd.Parameters.Add(add_parameters(par));
                    }

                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception e)
            {
                errorstr += " " + e.Message;
            }
        }

        public void executespreturnnd(string spname)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (Open_Connection())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = spname;
                }
                conn.Close();
            }
            catch (Exception e)
            {
                errorstr += " " + e.Message;
            }
        }
    }
}