﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DataAccessLayerr
{
    public class csParameterListType
    {
        private String _Name;

        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        private SqlDbType _SqlType;

        public SqlDbType SqlType
        {
            get { return _SqlType; }
            set { _SqlType = value; }
        }

        private Object _Value;

        public Object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public csParameterListType(string name, SqlDbType Stype, object value)
        {
            Name = name;
            SqlType = Stype;
            Value = value;
        }
    }
}