﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using BusinessLogicLayer;

namespace LMS.HR
{
    public partial class PersonalDetails : System.Web.UI.Page
    {
        bool det, qualif,entry,status;
        string id, name;

       PersonalDetail objp = new PersonalDetail();
       Lists lst = new Lists();       
       ProjectInfo proj = new ProjectInfo();
        
        protected void Page_Load(object sender, EventArgs e)
        {
         //   DropDownList dll = this.Page.FindControl("ddlrace") as DropDownList;
         //   ScriptManager.GetCurrent(this.Page).RegisterAsyncPostBackControl(dll);
            if (!Page.IsPostBack)
            {
                panall.Visible = false;
                pancomp.Visible = false;
                pansubmit.Visible = false;
                rblist.SelectedIndex = 0;
                pansearchid.Visible = true;

                try
                {
                    string idno = Request.QueryString["v"].ToString();
                    txtstudentsearch.Text = idno;
                    search_Click1(null, null);
                }
                catch (Exception ec)
                {

                }
            }
           
        }
        private void Clear()
        { 
            Panel uppanel = Master.FindControl("ContentPlaceHolder1").FindControl("upsearch").FindControl("panall") as Panel;

            foreach (var txbox in uppanel.Controls)
            {
                if (txbox is TextBox)
                    ((TextBox)txbox).Text = "";
                
            }
            foreach (var txbox in uppanel.Controls)
            {
                if (txbox is DropDownList)
                {
                   if (((DropDownList)txbox).DataSource ==null)
                      ((DropDownList)txbox).Text = "---Select---";
                }

            }
            //radioButton1.Checked = false;
            //radioButton2.Checked = false;
        }
      

        protected void butcreate_Click(object sender, EventArgs e)
        {
            
          if (CbId2.Checked == true && tbid.Text.Length != 13)
            {
                MessageAlert("Error", "Supply an Valid SA ID");
                return ;
            }

          string race, lang, nation, disab, hqualif, insti;
            
              race = ddlrace.SelectedItem.Text;
              if (race == "Others") race = tbrothers.Text;
            lang = ddllang.SelectedItem.Text;
            if (lang == "Others") lang = tblothers.Text;
                nation=ddlnation.SelectedItem.Text;
            if (nation == "Others") nation = tbnothers.Text;
                disab = ddldisab.SelectedItem.Text;
            if (disab == "Others") disab = tbdisab.Text;
                hqualif = ddlhqualf.SelectedItem.Text;
            if (hqualif == "Others") hqualif = tbhqualif.Text;
                insti = ddlinsti.SelectedItem.Text;
            if (insti == "Others") insti = tbinsti.Text;

       det=  objp.Save_Details(tbid.Text, tbfname.Text, tblname.Text,
                tbphone.Text,  txtemail.Text, tbphone2.Text, txthome.Text, race,ddlsex.SelectedItem.Text, 
                lang,nation, disab,ddlproj.SelectedItem.Text);

       qualif = objp.savequalification(tbid.Text, ddlproj.SelectedItem.Text, hqualif, insti);
           
            // get the project eligibility and check with the selected option by the learner.
          string elig =  proj.GetProjectEligibility(ddlproj.SelectedItem.Text);
          string [] eligibility = elig.Split(',');
          bool qualify=true;

          if (!eligibility.Contains(hqualif))
              qualify = false;
               
           if (pancomp.Visible == true)
            {
                string compname = ddlcomp.SelectedItem.ToString();
                string idno = tbid.Text;
           //     string compid = ddlid.SelectedItem.ToString();
                string unitstd = tbunitstd.Text;
                 entry = objp.SaveInterEntry(idno, compname, unitstd);
            }
            if ((det && qualif && pancomp.Visible == false) || (det && qualif && entry && pancomp.Visible == true))
            {
                Clear();
                panall.Visible = false;
                pansubmit.Visible = false;
                if (qualify)
                {
                    // save Personal Info in Interview List Table
                    if (lst.UpdateList(tbid.Text, "", "", "", ddlproj.SelectedItem.Text))
                        MessageAlert("SUCCESS!!!", "Information Saved Successfully");
                }
                else
                    MessageAlert("SUCCESS!!!", "Information Saved Successfully   You do not qulaify for the chosen Program!!!");
               

                status = true;
       
               // return ;
            }
            else
            { MessageAlert("ERROR!!!", "Error Saving Information!!!"); status = false; //return; 
            }
            // panlevel.Visible = false;
        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void search_Click1(object sender, EventArgs e)
        {
           
            PersonalDetail per = new PersonalDetail();
            

            if (string.IsNullOrEmpty(txtstudentsearch.Text))
            {
                MessageAlert("Error", "ID field Empty");
                return;
            }

            if (CbId1.Checked == true && txtstudentsearch.Text.Length != 13)
            {
                MessageAlert("Error", "Supply an Valid SA ID");
                return;
            }

         //   string search = per.Get_ID_Num(txtstudentsearch.Text);
            if (per.GetUserdetail(txtstudentsearch.Text))
            {
               
                tbid.Text = per.IDNo;
                id = per.IDNo;
                tbfname.Text = per.FName;
                tblname.Text = per.LName;
                name = per.FName + " " + per.LName;
                tbphone.Text = per.CellNo;
                tbphone2.Text = per.AltCellNo;
                txtemail.Text = per.Email;
                txthome.Text = per.HomeAdd;
              //  ddllang.Text = per.Lang;
             //   ddlnation.Text = per.Nation;
             //   ddlrace.Text = per.Race;
                ddlsex.Text = per.Gender;
             //   ddldisab.Text = per.Disability;
            //    ddlhqualf.Text = per.Qulif;
             //   ddlinsti.Text = per.Inst;
             //   ddlproj.Text = per.Projname;
               
                panall.Visible = true;
                pansubmit.Visible = true;

            }
            else
            {
                MessageAlert("Error!!!", "ID_Number Not Found");
                panall.Visible = false;
            }

        }

        protected void rblist_SelectedIndexChanged1(object sender, EventArgs e)
        {
            panall.Visible = false; 
            pansubmit.Visible = false; pancomp.Visible = false;
            if (rblist.SelectedItem.ToString() == "Search By ID")
            {
               pansearchid.Visible = true; panlevel.Visible = false; 
            }
            else
            {
                panlevel.Visible = true; pansearchid.Visible = false;  
            }
        }

        protected void rblentry_SelectedIndexChanged(object sender, EventArgs e)
        {
            panall.Visible = true; pancomp.Visible = false;
            pansubmit.Visible = true;
            Clear();
            if (rblentry.SelectedItem.ToString() == "Entry Level")
            {
               pancomp.Visible = false;
            }
            else
            {
                pancomp.Visible = true;
            }
            
        }

        protected void butsavexit2_Click(object sender, EventArgs e)
        {


            butcreate_Click(null, null);
           
            if(status)
            {
                // create session and redirect
                Session["LearnerId"] = tbid.Text;
                Session["LearnerName"] =tbfname.Text+" "+tblname.Text;
                Response.Redirect("../HR/NextOfKin.aspx?v=c");
            }
            else
                // panlevel.Visible = false;
                return;
        }

        protected void CbId1_CheckedChanged(object sender, EventArgs e)
        {
            if (CbId1.Checked)
            {
                txtstudentsearch.MaxLength = 13;
                txtstudentsearch_FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers;
            }
            else
            {
                txtstudentsearch.MaxLength = 25;
                FilteredTextBoxExtender ftbe = new FilteredTextBoxExtender();
                ftbe.FilterType = FilterTypes.Numbers | FilterTypes.LowercaseLetters | FilterTypes.UppercaseLetters;
                ftbe.TargetControlID = "txtstudentsearch";
               
            }
        }

        protected void CbId2_CheckedChanged(object sender, EventArgs e)
        {
           if (CbId2.Checked)
            {
                tbid.MaxLength = 13;
                tbid_FilteredTextBoxExtender.FilterType = AjaxControlToolkit.FilterTypes.Numbers;
            }
            else
            {
                tbid.MaxLength = 25;
                FilteredTextBoxExtender ftbe = new FilteredTextBoxExtender();
                ftbe.FilterType = FilterTypes.Numbers | FilterTypes.LowercaseLetters | FilterTypes.UppercaseLetters;
                ftbe.TargetControlID = "tbid";
            }
        }

        protected void ddlrace_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlrace.SelectedItem.ToString() == "Others")
                divrace.Visible = true;
            else divrace.Visible = false;
        }

        protected void ddllang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddllang.SelectedItem.ToString() == "Others")
                divlang.Visible = true;
            else divlang.Visible = false;
        }

        protected void ddlnation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlnation.SelectedItem.ToString() == "Others")
                divnation.Visible = true;
            else divnation.Visible = false;

        }

        protected void ddldisab_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddldisab.SelectedItem.ToString() == "Yes")
                divdisab.Visible = true;
            else divdisab.Visible = false;
        }

        protected void ddlhqualf_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlhqualf.SelectedItem.ToString() == "Others")
                divqualif.Visible = true;
            else divqualif.Visible = false;
        }

        protected void ddlinsti_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlinsti.SelectedItem.ToString() == "Others")
                divinsti.Visible = true;
            else divinsti.Visible = false;
        }

        protected void txtstudentsearch_TextChanged(object sender, EventArgs e)
        {
            panall.Visible = false; pancomp.Visible = false; pansubmit.Visible = false;
        }

        //protected void ddlcomp_DataBinding(object sender, EventArgs e)
        //{
        //    ddlcomp.Items.Insert(0, new ListItem("--Select--"));
        //    int cnt = ddlcomp.Items.Count;
        //    ddlcomp.Items.Add(new ListItem("Others"));
        //}

        //protected void ddlrace_DataBound(object sender, EventArgs e)
        //{

        //    DataListItem lb = e.Item.FindControl("LButHigh") as DataListItem;
        //    ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(lb);

        //}

             

     
    }
}