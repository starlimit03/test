﻿<%@ Page Theme="MyTheme"  Title="Next Of Kin Details" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true" CodeBehind="NextOfKin.aspx.cs" Inherits="LMS.HR.NextOfKin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 181px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="NOK">
<div class="clear hrspacer topmarg"> </div>
<table class="width80"  cellspacing="6px">
<tr class="pd_td"><td colspan="2"> <h4> NEXT OF KIN DETAILS</h4></td></tr>
<tr><td colspan="2"><hr class="hrspacer myhr" /></td></tr>
       <tr><td colspan="2"></td></tr>
            <tr class="pd_td" ><td>Identification Number : </td>
            <td align="center">
            <asp:TextBox ID="tbsid" runat="server"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;
            <asp:Button runat="server" ID="Buts" Text="SEARCH" 
                    onclick="Buts_Click" /> </td>
                    <td>
                 <%--   <asp:Label ID="lblsearcherror" runat="server"></asp:Label>--%>
                    </td></tr>
             <tr class="pd_td" ><td colspan="2">NAME : <asp:Label ID="lblname" runat="server"></asp:Label></td>
            </tr>
<tr class="pd_td"><td colspan="2"></td></tr>
<tr class="pd_td">
<td class="style1"> Name :</td><td><asp:TextBox ID="nokname" runat="server"></asp:TextBox></td></tr>

<tr class="pd_td">
<td class="style1"> Relationship :</td><td>
    <asp:DropDownList ID="ddlrlshp" runat="server">
    <asp:ListItem>----Select----</asp:ListItem>
    <asp:ListItem>Father</asp:ListItem>
    <asp:ListItem>Mother</asp:ListItem>
    <asp:ListItem>Brother</asp:ListItem>
    <asp:ListItem>Sister</asp:ListItem>
    <asp:ListItem>Spouse</asp:ListItem>
    </asp:DropDownList>
    </td></tr>
    
<tr class="pd_td"><td class="style1"> Contact Number :</td><td><asp:TextBox ID="tbcontact" runat="server"></asp:TextBox></td></tr>

<tr class="pd_td">
<td class="style1"> Alt. Contact Number :</td><td><asp:TextBox ID="tbcontact2" runat="server"></asp:TextBox></td></tr>

<tr class="pd_td">
<td class="style1"> Gender :</td><td>
<asp:DropDownList ID="ddlgender" runat="server">
    <asp:ListItem>----Select----</asp:ListItem>
    <asp:ListItem>Male</asp:ListItem>
    <asp:ListItem>Female</asp:ListItem>    </asp:DropDownList>
</td></tr>
<tr class="pd_td">
<td class="style1"> Address :</td><td>
    <asp:TextBox ID="tbadd" SkinID="TextArea" TextMode="MultiLine" Rows="8" 
        runat="server" Height="95px" Width="209px"></asp:TextBox>
</td></tr>
<tr class="pd_td">
<td class="style1"> T-Shirt Size :</td><td>
<asp:DropDownList ID="DropDownList1" runat="server">
    <asp:ListItem>----Select----</asp:ListItem>
    <asp:ListItem>X-Large</asp:ListItem>
    <asp:ListItem>Large</asp:ListItem>
    <asp:ListItem>Medium</asp:ListItem>
    <asp:ListItem>Small</asp:ListItem>     </asp:DropDownList>
    </td></tr>
    <tr class="pd_td"><td colspan="2"></td></tr>
<tr class="pd_td"><td align="center" colspan="2"><asp:Button id="butsave" 
        runat="server" Text="SAVE" onclick="butsave_Click" /> </td></tr>
        <tr class="pd_td msglabel"><td align="center" colspan="2"><asp:Label ID="lblMsg" runat="server"></asp:Label> </td></tr>
</table>

<div class="bottommarg"></div>
</div>
</asp:Content>
