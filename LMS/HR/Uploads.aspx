﻿<%@ Page Title="Document Uploads" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Uploads.aspx.cs" Inherits="LMS.HR.Uploads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="UP">
        <div class="clear hrspacer topmarg">
        </div>
        <table width="100%" cellspacing="6px">
            <tr class="pd_td">
                <td colspan="2">
                    <h4>
                        DOCUMENT UPLOADS</h4>
                    <hr class="myhr" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Identification Number :
                    <asp:TextBox ID="tbsid" runat="server"></asp:TextBox>
                </td>
                <td align="center">
                    <asp:Button runat="server" ID="Buts" Text="SEARCH" />
                </td>
            </tr>
            <tr class="pd_td">
                <td colspan="2">
                    NAME :
                    <asp:Label ID="lblname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Profile Picture
                </td>
                <td>
                    <asp:FileUpload ID="FUpix" runat="server" />
                </td>
                <td>
                    <asp:Button ID="Picupload" runat="server" Text="Upload" OnClick="Picupload_Click" />
                </td>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="style1">
                        &nbsp;&nbsp;<asp:Label ID="lblStats1" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="sn1" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click1"></asp:LinkButton>
                    </td>
                </tr>
            </tr>
            <tr class="pd_td">
                <td>
                    Curriculum Vitae
                </td>
                <td>
                    <asp:FileUpload ID="FUCv" runat="server" />
                </td>
                <td>
                    <asp:Button ID="Cvupload" runat="server" Text="Upload" OnClick="Cvupload_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblStats2" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="sn2" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Motivation Letter
                </td>
                <td>
                    <asp:FileUpload ID="FUML" runat="server" />
                </td>
                <td>
                    <asp:Button ID="Letterupload" runat="server" Text="Upload" OnClick="Letterupload_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblStats3" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="sn3" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Matric
                </td>
                <td>
                    <asp:FileUpload ID="FUTC" runat="server" />
                </td>
                <td>
                    <asp:Button ID="Matricupload" runat="server" Text="Upload" OnClick="Matricupload_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblStats4" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="sn4" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Certificate
                </td>
                <td>
                    <asp:FileUpload ID="FUcert" runat="server" />
                </td>
                <td>
                    <asp:Button ID="certUpload" runat="server" Text="Upload" OnClick="certUpload_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Others
                </td>
                <td>
                    <asp:FileUpload ID="FUodas" runat="server" />
                </td>
                <td>
                    <asp:Button ID="otherupload" runat="server" Text="Upload" OnClick="otherupload_Click" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label4" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click"></asp:LinkButton>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Previous Programming Language :
                </td>
                <td>
                    <asp:TextBox ID="tbplang" TextMode="MultiLine" SkinID="TextArea" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr class="pd_td">
                <td align="center" colspan="3">
                    <asp:Button ID="Butsave" Text="SAVE" runat="server" OnClick="Butsave_Click" />
                </td>
            </tr>
            <tr class="center msglabel" >
                <td colspan="3">
                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                </td>
              </tr>
        </table>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
