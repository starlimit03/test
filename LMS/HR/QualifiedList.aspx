﻿<%@ Page Title="Qualified Candidates" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="QualifiedList.aspx.cs" Inherits="LMS.HR.QualifiedList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #imgcal
        {
            width: 28px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="QL">
        <div class="topmarg">
        </div>
        <h4>
            LIST OF ALL QUALIFIED CANDIDATES</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
      <asp:UpdatePanel ID="upgrid" runat="server" UpdateMode="Always">
                <ContentTemplate>
        <asp:Panel ID="panselect" runat="server">
            <table class="width80">
                <tr class="pd_td">
                    <td>
                    </td>
                </tr>
                <tr class="pd_td">
                    <td colspan="2">
                        <asp:RadioButtonList AutoPostBack="true" ID="rblist" runat="server" 
                            onselectedindexchanged="rblist_SelectedIndexChanged">
                            <asp:ListItem>Search By ID</asp:ListItem>
                            <asp:ListItem>Search By Program</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
       <asp:Panel ID="panid" runat="server" Visible="false">
            <table class="width80" cellspacing="5">
                <tr class="center pd_td">
                    <td colspan="4">
                      <h3>Search Criteria</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr class="hrspacer myhr" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Identification Number :
                    </td>
                    <td>
                        <asp:TextBox ID="tbsearchid" runat="server"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="true" ControlToValidate="tbsearchid" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                   <%--<asp:FilteredTextBoxExtender ID="filterbx" runat="server" FilterType="Numbers,UppercaseLetters,LowercaseLetters" FilterMode="ValidChars" ></asp:FilteredTextBoxExtender>
                   --%> </td>
                <td>    <asp:Button ID="Butsearch2" runat="server" Text="SEARCH" OnClick="butsearch_Click" /></td>
                </tr>
                <tr> <td colspan="4">
                        <hr class="hrspacer " />
                    </td></tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pangrp" runat="server" Visible="false">
            <table class="width80" cellspacing="5">
                <tr class="center pd_td">
                    <td colspan="4">
                     <h3>Search Criteria</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <hr class="hrspacer myhr" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Group :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlgrp" runat="server" DataSourceID="SqlDataSource1" DataTextField="ProjName"
                            DataValueField="ProjName" AppendDataBoundItems="true">
                            <asp:ListItem Value="---Select---">---Select---</asp:ListItem>
                            <asp:ListItem Value="All Groups">All Groups</asp:ListItem>
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlgrp"
                            InitialValue="---Select---" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                   
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       <%-- <asp:CheckBox ID="cball" runat="server" CssClass="msglabel" AutoPostBack="true" OnCheckedChanged="cball_checked" Text="All Groups" />
                       --%>
                        <asp:SqlDataSource ID="SqlDataSource1"  runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                            SelectCommand="GetAllProject_SP" SelectCommandType="StoredProcedure" >
                              <SelectParameters>
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="pname" Type="String" />
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="type" Type="String" />
                                    </SelectParameters>
                             </asp:SqlDataSource>
                    </td>
                       <td>
                        Interview Status :
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddltype">
                            <asp:ListItem>---Select---</asp:ListItem>
                            <asp:ListItem>Scheduled</asp:ListItem>
                            <asp:ListItem>Unscheduled</asp:ListItem>
                            <asp:ListItem>Both</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddltype"
                            InitialValue="---Select---" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                 
                <tr>
                    <td colspan="4">
                        <hr class="hrspacer" />
                    </td>
                </tr>
                <tr>
                 
                    <td colspan="4" class="center">
                        <asp:Button ID="butsearch" runat="server" Text="SEARCH" OnClick="butsearch_Click" />
                    </td>
                </tr>
                <tr> <td colspan="4">
                        <hr class="hrspacer" />
                    </td></tr>
            </table>
        </asp:Panel>
    <%--    <asp:Panel ID="pansearch" runat="server">
           <table class="width80" cellspacing="5">
               
                <tr class="pd_td">
                    <td>
                    </td>
                </tr>
            </table>
        </asp:Panel>--%>
        <asp:Panel runat="server" ID="pangrid" >
           <div id="gridscroll" >
              <asp:GridView SkinID="Gridall"  ID="GridView1" Style="margin: auto" DataKeyNames="ID_Num"  runat="server"
                        OnPageIndexChanging="GridView1_PageIndexChanging" >
                        <Columns>
                            <asp:TemplateField HeaderText="SN" InsertVisible="False" SortExpression="SN">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Identification No." SortExpression="IdNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblid" runat="server" Text='<%# Bind("ID_Num") %>'></asp:Label>
                                </ItemTemplate>
                             
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="120px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                                NullDisplayText="&quot;No data&quot;">
                                <ItemStyle HorizontalAlign="Center" Width="150px" VerticalAlign="Middle" />
                            </asp:BoundField>
                            
                            <asp:BoundField DataField="Qualif" HeaderText="Qualification" 
                                SortExpression="Qualif">
                                <ItemStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle" />
                            </asp:BoundField>
                            <asp:HyperLinkField DataNavigateUrlFields="ID_Num" DataNavigateUrlFormatString="PersonalDetails.aspx?v={0}"
                                HeaderText="View Candidate" Text="View">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:HyperLinkField>
                            <asp:TemplateField HeaderText="Project" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblprjname" runat="server" Text='<%# Bind("ProjName") %>'></asp:Label>
                                </ItemTemplate>
                              
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Schedule for Interview" >
                                  <ItemTemplate>
                                    <asp:CheckBox ID="Cbschedule" OnCheckedChanged="Cbschedule_OnCheckedChanged"  AutoPostBack="true" runat="server" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" >
                                <ItemTemplate>
                                    <asp:Label ID="lbldate" runat="server" Text='<%# Bind("InterDate") %>'></asp:Label>
                                </ItemTemplate>
                               
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Time" ItemStyle-Width="120">
                                <ItemTemplate>
                                    <asp:Label ID="lbltime" runat="server" Text='<%# Eval("InterTime") %>'></asp:Label>
                                    <asp:TextBox Visible="false" SkinID="GridViewTb" ID="tbtime" runat="server" Text='<%# Eval("InterTime") %>'> runat="server"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Font-Size="XX-Small" SetFocusOnError="true"
                                        runat="server" ErrorMessage="24Hr Format" ValidationExpression="^([01]?[0-9]|2?[0-3])[\:]([0-5][0-9])$" ForeColor="Red"
                                        ControlToValidate="tbtime"></asp:RegularExpressionValidator>
                                        <asp:FilteredTextBoxExtender FilterMode="ValidChars" TargetControlID="tbtime" FilterType="Custom" runat="server" ValidChars="0123456789:"></asp:FilteredTextBoxExtender>
                                    <asp:RangeValidator ID="RangeValidator1" MaximumValue="17:00" MinimumValue="07:00" Font-Size="XX-Small" ForeColor="Red" SetFocusOnError="True" ControlToValidate="tbtime" runat="server" ErrorMessage="7:00-17:00"></asp:RangeValidator>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Venue">
                             <ItemTemplate>
                                    <asp:Label ID="lblvenue" runat="server" Text='<%# Bind("Venue") %>'></asp:Label>
                                </ItemTemplate>
                               
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="120px" /></asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Interview Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblstat" runat="server" Text='<%# Eval("InterStatus") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlstatus" runat="server">
                                        <asp:ListItem>Scheduled</asp:ListItem>
                                        <asp:ListItem>Unscheduled</asp:ListItem>
                                        <asp:ListItem>Interviewed</asp:ListItem>
                                        <asp:ListItem>Declined</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                            </asp:TemplateField>
                         
                        </Columns>
                        <AlternatingRowStyle BackColor="#EBFDD4" />
                        <RowStyle BackColor="#BEF8CF" />
                        <PagerStyle CssClass="TextboxShadow" />
                    </asp:GridView>
                    <%--<asp:SqlDataSource ID="SqlDataSource1" runatS="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>--%>
               
        </div>
   </asp:Panel>
        <div class="clear hrspacer">
        </div>
        <asp:Panel ID="pansubmit" runat="server" Visible="false">
        <table cellspacing="6px" class="width50">
            <tr class="pd_td">
                <td>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Schedule Date :
                </td>
                <td>
                    <asp:TextBox ID="tbcal" runat="server" Enabled="false"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="tbcal" runat="server"
                        DaysModeTitleFormat="d MMMM, yyyy" PopupButtonID="imgcal" PopupPosition="TopRight"
                        TodaysDateFormat="d MMMM, yyyy" FirstDayOfWeek="Sunday" Enabled="True">
                    </asp:CalendarExtender>
                    &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcal" height="25px" />
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Venue :
                </td>
                <td>
                    <asp:TextBox ID="tbven" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr class="pd_td">
                <td align="center">
                    <asp:Button ID="butsch" runat="server" Text="SCHEDULE" onclick="butsch_Click" />
                </td>
                <td align="center">
                    &nbsp;</td>
            </tr>
        </table>
        </asp:Panel>
          </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="rblist" />
                </Triggers>
            </asp:UpdatePanel>
          <asp:Panel runat="server" ID="panprint">  
            <h4>  <asp:Button ID="butprint" runat="server" 
    Text="PRINT LIST" />
                </h4>        </asp:Panel><div class="bottommarg">
        </div>
    </div>
</asp:Content>
