﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using BusinessLogicLayer;
using System.Web.UI.WebControls;

namespace LMS.HR
{
    public partial class NextOfKin : System.Web.UI.Page
    {
        string id,name;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string desturl = Request.QueryString["v"].ToString();
                tbsid.Text = Session["LearnerId"].ToString();
                lblname.Text = Session["LearnerName"].ToString();
                MessageAlert("Success!", "Information Successfully Saved!");
            }
            catch (Exception ec)
            {
            }
           //if (string.IsNullOrEmpty(desturl) == false) // redirected page
           //{
           //    tbsid.Text = Session["LearnerId"].ToString();
           //    lblname.Text = Session["LearnerName"].ToString();

           //}

        }

        protected void Buts_Click(object sender, EventArgs e)
        {
            
            PersonalDetail per = new PersonalDetail();

            

            string search = per.Get_ID_Num(tbsid.Text);
            if (search == tbsid.Text)
            {
                per.GetUserdetail(search);
                lblname.Text = per.FName;

            }
            else
                MessageAlert("Error", "Student Not Found");
        }

        protected void butsave_Click(object sender, EventArgs e)
        {
            NextKin next=new NextKin ();
            if (next.SaveNextOfKin(tbsid.Text, nokname.Text, ddlrlshp.SelectedItem.Text, tbcontact.Text, tbcontact2.Text, ddlgender.SelectedItem.Text, tbadd.Text, DropDownList1.SelectedItem.Text))
            {
                lblMsg.Text = "Information Successfully Saved";
                MessageAlert("SUCCESS!!!", "Information Successfully Saved");
            }
            else
            {
                lblMsg.Text = "Error Saving Information";
                MessageAlert("ERROR!!!", "Error Saving Information");
            }
        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }
    }
}