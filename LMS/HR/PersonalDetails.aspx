﻿<%@ Page Title="Learners Personal Details" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="PersonalDetails.aspx.cs" Inherits="LMS.HR.PersonalDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
         
    </script>
    <style type="text/css">
        .style1
        {
            width: 197px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PD">
        <h4 class="topmarg">
            PERSONAL DETAILS
        </h4>
        <hr class="hrspacer myhr" />
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upsearch">
            <ContentTemplate>
                <asp:Panel ID="panselect" runat="server">
                    <table class="width80">
                        <tr class="pd_td">
                            <td>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="2">
                                <asp:RadioButtonList AutoPostBack="true" ID="rblist" runat="server" OnSelectedIndexChanged="rblist_SelectedIndexChanged1">
                                    <asp:ListItem>Search By ID</asp:ListItem>
                                    <asp:ListItem>Add New Entry</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="panlevel" Visible="false">
                    <asp:RadioButtonList ID="rblentry" AutoPostBack="true" CssClass="width50" runat="server"
                        OnSelectedIndexChanged="rblentry_SelectedIndexChanged">
                        <asp:ListItem>Entry Level</asp:ListItem>
                        <asp:ListItem>Intermediate Level</asp:ListItem>
                    </asp:RadioButtonList>
                </asp:Panel>
                <asp:Panel ID="pansearchid" runat="server" Visible="false">
                    <table class="width90">
                        <tr class="pd_td">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Identification Number :
                                <asp:TextBox ID="txtstudentsearch" runat="server" AutoPostBack="true" 
                                    ontextchanged="txtstudentsearch_TextChanged"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="txtstudentsearch_FilteredTextBoxExtender" FilterType="LowercaseLetters,UppercaseLetters,Numbers"
                                    runat="server" Enabled="True" TargetControlID="txtstudentsearch">
                                </asp:FilteredTextBoxExtender>
                                <%--                       <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="CbId1" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td class="msglabel">
                                <asp:CheckBox ID="CbId1" Text="South African ID ?" AutoPostBack="true" runat="server"
                                    OnCheckedChanged="CbId1_CheckedChanged" />
                            </td>
                            <td>
                                <asp:Button Width="150px" Height="25px" CssClass="but" ID="butsearch" runat="server" Text="SEARCH"
                                    OnClick="search_Click1" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="panall" Visible="false" runat="server">
                    <table style="height: auto;" class="width80" cellspacing="5px">
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Identification Number :
                            </td>
                            <td>
                                <asp:TextBox ID="tbid" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbid_FilteredTextBoxExtender" runat="server" FilterType="LowercaseLetters,UppercaseLetters,Numbers"
                                    Enabled="True" TargetControlID="tbid">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ControlToValidate="tbid"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td class="msglabel">
                                <asp:CheckBox ID="CbId2" Text="South African ID ?" AutoPostBack="true" runat="server"
                                    OnCheckedChanged="CbId2_CheckedChanged" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                First Name :
                            </td>
                            <td>
                                <asp:TextBox ID="tbfname" MaxLength="15" runat="server" Width="120px"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbfname_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" TargetControlID="tbfname" FilterType="UppercaseLetters,LowercaseLetters">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbfname"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Last Name :
                            </td>
                            <td>
                                <asp:TextBox ID="tblname" MaxLength="15" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tblname_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tblname">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tblname"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Cell Number :
                            </td>
                            <td>
                                <asp:TextBox ID="tbphone" MaxLength="10" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="tbphone">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="tbphone"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Alternate Cell Number :
                            </td>
                            <td>
                                <asp:TextBox ID="tbphone2" MaxLength="10" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="tbphone2">
                                </asp:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Email Address :
                            </td>
                            <td>
                                <asp:TextBox ID="txtemail" runat="server" Width="120px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtemail"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Home Address :
                            </td>
                            <td>
                                <asp:TextBox ID="txthome" runat="server" TextMode="MultiLine"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" Enabled="True"
                                    TargetControlID="txthome" FilterMode="InvalidChars" InvalidChars="!@#$%^&amp;*()_&lt;&gt;?|~}{[]\';:">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ControlToValidate="txthome"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Race :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlrace" AutoPostBack="true" runat="server" Height="16px" Width="117px"
                                    OnSelectedIndexChanged="ddlrace_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Black</asp:ListItem>
                                    <asp:ListItem>White</asp:ListItem>
                                    <asp:ListItem>Asian</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator7"
                                    ControlToValidate="ddlrace" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divrace" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tbrothers" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tbrothers">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Gender :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlsex" runat="server" Height="16px" Width="117px">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Male</asp:ListItem>
                                    <asp:ListItem>Female</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator8"
                                    ControlToValidate="ddlsex" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Home Language :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddllang" AutoPostBack="true" runat="server" Height="16px" Width="117px"
                                    OnSelectedIndexChanged="ddllang_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Isuzulu</asp:ListItem>
                                    <asp:ListItem>English</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator9"
                                    ControlToValidate="ddllang" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divlang" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tblothers" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tblothers">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Nationality :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlnation" AutoPostBack="true" runat="server" Height="16px"
                                    Width="117px" OnSelectedIndexChanged="ddlnation_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>South African</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator10"
                                    ControlToValidate="ddlnation" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divnation" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tbnothers" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tbnothers">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Disability :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddldisab" AutoPostBack="true" runat="server" Height="16px"
                                    Width="117px" OnSelectedIndexChanged="ddldisab_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator11"
                                    ControlToValidate="ddldisab" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divdisab" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tbdisab" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tbdisab">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td >
                                Project Applied for :
                            </td>
                            <td colspan="3" align="left">
                                <asp:DropDownList ID="ddlproj" runat="server" DataSourceID="SqlDataSource2" DataTextField="ProjName"
                                    DataValueField="ProjName" AppendDataBoundItems="true">
                                    <asp:ListItem >---Select---</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlproj"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                                    SelectCommand="GetAllProject_SP" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="pname" Type="String" />
                                        <asp:Parameter DefaultValue="ByName" Name="type" Type="String" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <h4>
                                    EDUCATIONAL BACKGROUND</h4>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Highest Qualification Obtained :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlhqualf" AutoPostBack="true" runat="server" Height="16px"
                                    Width="117px" OnSelectedIndexChanged="ddlhqualf_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>Masters</asp:ListItem>
                                    <asp:ListItem>Honors</asp:ListItem>
                                    <asp:ListItem>N.Dip</asp:ListItem>
                                    <asp:ListItem>Certificate</asp:ListItem>
                                    <asp:ListItem>College</asp:ListItem>
                                    <asp:ListItem>Matric</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator13"
                                    ControlToValidate="ddlhqualf" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divqualif" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tbhqualif" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tbhqualif">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                                Institution Obtatined :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlinsti" AutoPostBack="true" runat="server" Height="16px"
                                    Width="117px" OnSelectedIndexChanged="ddlinsti_SelectedIndexChanged">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                    <asp:ListItem>DUT</asp:ListItem>
                                    <asp:ListItem>Vaal</asp:ListItem>
                                    <asp:ListItem>UJ</asp:ListItem>
                                    <asp:ListItem>TUT</asp:ListItem>
                                    <asp:ListItem>Others</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator6"
                                    ControlToValidate="ddlinsti" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divinsti" visible="false">
                                <td>
                                    Specify :
                                </td>
                                <td>
                                    <asp:TextBox ID="tbinsti" runat="server" Width="120px"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" Enabled="True"
                                        FilterType="UppercaseLetters,LowercaseLetters" TargetControlID="tbinsti">
                                    </asp:FilteredTextBoxExtender>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td center">
                            <td colspan="3">
                                <%--   <asp:Button ID="butcreate" runat="server" Text="CREATE" OnClick="butcreate_Click" />--%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pancomp" Visible="false" runat="server">
                    <table class="width90" cellspacing="6">
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Company Name :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlcomp" runat="server"  DataSourceID="SqlDataSource1" 
                                    DataTextField="CompName" DataValueField="CompName"  AppendDataBoundItems="true">
                                    <asp:ListItem >---Select---</asp:ListItem>
                                </asp:DropDownList>
                                          <asp:RequiredFieldValidator InitialValue="---Select---" ID="RequiredFieldValidator16" ControlToValidate="ddlcomp" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                                    SelectCommand="GetAllCompany" SelectCommandType="StoredProcedure">
                                </asp:SqlDataSource>
                            
                            </td>
   <td colspan="2" class="center">
                                Unit Standards Covered :
                            </td>
                            <td >
                                <asp:TextBox runat="server" ID="tbunitstd"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbunitstd"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" Enabled="True"
                                FilterType="UppercaseLetters,Numbers,LowercaseLetters" TargetControlID="tbunitstd">
                            </asp:FilteredTextBoxExtender>
                        </tr>
                        
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" Visible="false" ID="pansubmit">
                    <table class="width90">
                        <tr class="pd_td">
                            <td>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="3">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="center">
                                <asp:Button ID="butsav2" CssClass="but" OnClick="butcreate_Click" runat="server" Text="SAVE & EXIT" />
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="butsavexit2" CssClass="but" runat="server" Text="SAVE & CONTINUE" OnClick="butsavexit2_Click" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="rblist" />
                <asp:AsyncPostBackTrigger ControlID="rblentry" />
                <asp:AsyncPostBackTrigger ControlID="butsearch" />
                <asp:AsyncPostBackTrigger ControlID="CbId1" />
                <asp:AsyncPostBackTrigger ControlID="CbId2" />
                <asp:AsyncPostBackTrigger ControlID="ddlrace" />
                <asp:AsyncPostBackTrigger ControlID="ddllang" />
                <asp:AsyncPostBackTrigger ControlID="ddlnation" />
                <asp:AsyncPostBackTrigger ControlID="ddlinsti" />
                <asp:AsyncPostBackTrigger ControlID="ddlhqualf" />
                <asp:AsyncPostBackTrigger ControlID="ddlcomp" />
                <asp:AsyncPostBackTrigger ControlID="txtstudentsearch" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="pd_td">
        </div>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
