﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BusinessLogicLayer;

namespace LMS.HR
{
    public partial class QualifiedList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // BindGrid();
                   pangrid.Visible = false;
                    pangrp.Visible = false;
                    pansubmit.Visible = false;
                    rblist.SelectedIndex = 0;
                    panid.Visible = true;
                    panprint.Visible = false;

            }
        }
        //private void BindGrid()
        //{
        //    SqlCommand cmd = new SqlCommand("SELECT * FROM testing");
        //    GridView1.DataSource = this.ExecuteQuery(cmd, "SELECT");
        //    GridView1.DataBind();
        //}

        //private DataTable ExecuteQuery(SqlCommand cmd, string action)
        //{
        //    string conString = ConfigurationManager.ConnectionStrings["testingConnectionString"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(conString))
        //    {
        //        cmd.Connection = con;
        //        switch (action)
        //        {
        //            case "SELECT":
        //                using (SqlDataAdapter sda = new SqlDataAdapter())
        //                {
        //                    sda.SelectCommand = cmd;
        //                    using (DataTable dt = new DataTable())
        //                    {
        //                        sda.Fill(dt);
        //                        return dt;
        //                    }
        //                }
        //            case "UPDATE":
        //                con.Open();
        //                cmd.ExecuteNonQuery();
        //                con.Close();
        //                break;
        //        }
        //        return null;
        //    }
        //}

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

     static string status = "", group = "", id = "", type = "";

        protected void butsearch_Click(object sender, EventArgs e)
        {
            
            Lists lst = new Lists();
            
          if (panid.Visible == true)
            {
                if (string.IsNullOrEmpty(tbsearchid.Text) == true)
                { MessageAlert("Error", "Identication Number Field Empty!"); return; }
                type = "idonly";
                id = tbsearchid.Text;
            }

          if (pangrp.Visible == true)
          {
              group = ddlgrp.SelectedItem.Text;

              if (group =="All Groups") type = "grp_listal";
              else type = "grp_list";
          }
                status = ddltype.SelectedItem.Text;
          

            DataSet ds=lst.GetInterList(id, group, status, type);
            if (ds!=null)
            {
                // Populate Gridview with Info;
                GridView1.DataSource = ds;
                GridView1.DataBind();
                pangrid.Visible = true;
               pansubmit.Visible = false;
               panprint.Visible = true;

            }
            else
            {
                MessageAlert("ERROR", "No Data for selected Search!!!");
                panprint.Visible = false;
            }


        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void rblist_SelectedIndexChanged(object sender, EventArgs e)
        {
          //  pansearch.Visible = false;
            pangrid.Visible = false; 
            if (rblist.SelectedItem.Text == "Search By ID")
            { panid.Visible = true; pangrp.Visible = false; } //pansearch.Visible = true; 
            else
            { panid.Visible = false; pangrp.Visible = true; } //pansearch.Visible = true;

        }

        protected void Cbschedule_OnCheckedChanged(object sender, EventArgs e)
        {
            int y=0;
            CheckBox cb = (CheckBox)sender;
            GridViewRow gvr = (GridViewRow)cb.NamingContainer;
            TextBox tb = (TextBox)gvr.FindControl("tbtime");
            Label lb = (Label)gvr.FindControl("lbltime");

            if (cb.Checked)
            {
                lb.Visible = false;
                tb.Visible = true;
                // GridView1.UpdateRow(
            }
            else
            {
                lb.Visible = true;
                tb.Visible = false;
            }
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("Cbschedule"));

                if (cbc.Checked)
                {
                    y++;
                }
            }
            if (y > 0)
                pansubmit.Visible = true;
            else pansubmit.Visible = false;
        }

        //private bool tablecopied = false;
        //private DataTable origdatatable;
        //private string  time,id;

        //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //     if(!tablecopied)
        //    {
        //        origdatatable = ((DataRowView)e.Row.DataItem).Row.Table.Copy();
        //        ViewState["originalvaluesdatatable"] = origdatatable;
        //        tablecopied = true;
        //    }

        //}

        protected void butsch_Click(object sender, EventArgs e)
        {
            int i = 0,x=0; string idnum="",timet, datet,venue,projname,status,msg="";
            Lists lst = new Lists();
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cb = ((CheckBox)r.FindControl("Cbschedule"));
             
                if (cb.Checked)
                {
                    status = ((Label)r.FindControl("lblstat")).Text;
                     datet = ((Label)r.FindControl("lbldate")).Text;
                     venue = ((Label)r.FindControl("lblvenue")).Text;
                     timet = ((TextBox)r.FindControl("tbtime")).Text;
                     projname = ((Label)r.FindControl("lblprjname")).Text;

                     idnum = ((Label)r.FindControl("lblid")).Text;
                     if (status == "UnScheduled")
                     {
                         if (tbcal.Text == string.Empty || tbven.Text == string.Empty || timet == string.Empty)
                         {
                             x++; msg = "Field not supplied for " + x + "Rows";
                             continue;
                         }
                     }
                  if (tbcal.Text != string.Empty) 
                        datet = tbcal.Text;
                  if (tbven.Text != string.Empty)
                      venue = tbven.Text;

                  if (lst.UpdateList(idnum, datet, timet,venue,projname))
                      i++;
                }
            }
            if (i > 0)
            {
                MessageAlert("Info", i + " Rows Successfully Scheduled!" + "  " + msg);
               
            }
            else MessageAlert("Info", "No Row(s) Scheduled!");

            butsearch_Click(null, null);
            //DataSet ds = lst.GetInterList(idnum, group, status, type);
            //if (ds != null)
            //{
            //    // Populate Gridview with Info;
            //    GridView1.DataSource = ds;
            //    GridView1.DataBind();
            //    pangrid.Visible = true;
            //   pansubmit.Visible = false;
            //   panprint.Visible = true;
            //}
           
        }

        //protected void cball_checked(object sender, EventArgs e)
        //{
        //    if (cball.Checked)
        //        ddlgrp.Enabled = false;
        //    else
        //        ddlgrp.Enabled = true;
        //}

        //protected void butsch_Click2(object sender, EventArgs e)
        //{
        //    int i = 0;
        //    origdatatable = (DataTable)ViewState["originalvaluesdatatable"];
        //    Lists lst = new Lists();
        //    foreach(GridViewRow r in GridView1.Rows)
        //        if (IsModified(r))
        //        {
                    
        //            if (lst.UpdateList(id, tbcal.Text, time,""))
        //                i++;
        //           // GridView1.UpdateRow(r.RowIndex, false);

        //        }
        //    if (i > 0)
        //    {
        //        MessageAlert("Info", i + " Rows Successfully Updated!");
                
        //    }
        //    else MessageAlert("Info", "No Row(s) Updated!");
        //    tablecopied = false;
        //    GridView1.DataBind();
        //    pangrid.Visible = true;
        //}

        //protected bool IsModified(GridViewRow r)
        //{
        // string currentid;
        //    string currenttime;
        //   // Int32.TryParse(GridView1.DataKeys[r.RowIndex].Value.ToString(), out currentid);
        //   currentid = GridView1.DataKeys[r.RowIndex].Value.ToString();
        //    currenttime = ((TextBox)r.FindControl("tbtime")).Text;
        //    time = currenttime;
        //    id = currentid;
            
        //   DataRow row = origdatatable.Select(string.Format("ID_Num = '{0}'", currentid))[0];

        //    if (!currenttime.Equals(row["InterTime"].ToString())) { return true; }
        //    return false;
        //}
    }
}