﻿<%@ Page Title="Termination of Contracts" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true"
    CodeBehind="CTermination.aspx.cs" Inherits="LMS.HR.CTermination" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="CT">
        <div class="topmarg">
        </div>
        <h4>
          TERMINATION OF CONTRACT</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <table cellspacing="6" class="width80">
            <tr class="pd_td">
                <td>
                    Termination Date : 
                    <asp:TextBox ID="tbdate" runat="server" Enabled="false"></asp:TextBox>
                    &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" class="imgcal" id="img1" />
                    <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="tbdate" runat="server"
                        DaysModeTitleFormat="d MMMM, yyyy" PopupButtonID="img1" PopupPosition="TopRight"
                        TodaysDateFormat="d MMMM, yyyy" FirstDayOfWeek="Sunday" Enabled="True">
                    </asp:CalendarExtender>
           
                </td>
                <td>
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                </td>
            </tr>
        </table>
        <div id="gridscroll">
            <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                DataSourceID="SqlDataSource1" >
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" ReadOnly="True" SortExpression="SN">
                        <ItemStyle Width="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification No." SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Exit Interview Done?" SortExpression="IdNo">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Textbook Returned?" SortExpression="IdNo">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Program Completed?" SortExpression="IdNo">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox3" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox3" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Reason(s)">
                        <ItemTemplate>
                            <asp:TextBox ID="comm" Columns="7" TextMode="MultiLine" SkinID="GridTxtArea" runat="server"
                                Rows="3"></asp:TextBox>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                </Columns>
                                <AlternatingRowStyle BackColor="#EBFDD4"  />
                <RowStyle BackColor="#BEF8CF" />
                 <PagerStyle CssClass="TextboxShadow" />
               </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        <div class="clear hrspacer">
        </div>
        <p style="text-align: center">
            <asp:Button ID="butsave" runat="server" Text="SAVE" /></p>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
