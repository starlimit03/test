﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using BusinessLogicLayer;

namespace LMS.HR
{
    public partial class Uploads : System.Web.UI.Page
    {
        FileUploadSession dat = new FileUploadSession();
        //Session[] data = new Session[6];

        //     private string Ctrl = "";
        Session[] data = new Session[6];

        int id = 6867;
        string name, contentType; int size; byte[] fileData;



        protected void Page_Load(object sender, EventArgs e)
        {
            string id = "6867876878";
            

            sn1.Visible = false;
            sn2.Visible = false;
            sn3.Visible = false;
            sn4.Visible = false;
          
            lblResult.Text = "";
            lblStats1.Text = string.Empty;// data[0].SName.ToString();
            lblStats2.Text = string.Empty; //data[1].SName.ToString();
            lblStats3.Text = string.Empty; //data[2].SName.ToString();

            if (!Page.IsPostBack)
            {
                for (int i = 0; i < data.Length; i++)
                    data[i] = new Session();
                Session["data"] = data;// fileInfo;


                data = dat.GetInfo(id);
                if (data != null)//.GetInfo(3)
                {
                    Session["data"] = data;
                    //   LblStat1.Text = data;
                    Butsave.Text = "UPDATE";
                    LinkButton1.Text = data[0].SName.ToString();
                    LinkButton2.Text = data[1].SName.ToString();
                    LinkButton3.Text = data[2].SName.ToString();
                    LinkButton4.Text = data[3].SName.ToString();
                    sn1.Text = data[0].SN.ToString();
                    sn2.Text = data[1].SN.ToString();
                    sn3.Text = data[2].SN.ToString();
                    sn4.Text = data[3].SN.ToString();
                    Label2.Text = data[4].SN.ToString();
                    Label4.Text = data[5].SN.ToString();

                    string idno = Session["id"].ToString();
                }
            }

        }
        protected bool ScanFolder(Session[] test, int id)
        {
            // Scanning file in the folder
            bool found = false;
            DirectoryInfo myfolder = new DirectoryInfo(Server.MapPath("~/Documents/"));
            //loop through each file in the folder

            foreach (FileInfo fileData in myfolder.GetFiles())
            {
                string files = fileData.Name;
                if (files.Contains(id.ToString()))
                {
                    bool match = false;
                    for (int i = 0; i < test.Length; i++)
                    {
                        if (files == id + test[i].SName)
                        { match = true; break; }

                    }
                    if (!match) fileData.Delete();
                    found = true;
                }
            }

            return found;
        }
        
        protected int UpLoad(string Ctrl, int type, string filesent)
        {
            // Control nam= FindControl(Ctrl);
            Ctrl = Ctrl.Substring(20);

            FileUpload fileup = Master.FindControl("ContentPlaceHolder1").FindControl(Ctrl) as FileUpload;

            if (fileup.HasFile)
            {
                if (fileup.PostedFile.ContentLength < 1012000)
                {
                    try
                    {

                        //fielname = "CV";
                        //  name = fileup.FileName;
                        size = fileup.PostedFile.ContentLength;

                        contentType = fileup.PostedFile.ContentType;

                        fileData = new byte[size];
                        fileup.PostedFile.InputStream.Read(fileData, 0, size);
                        // int button=1;


                        data = Session["data"] as Session[];

                        name = Path.GetFileName(fileup.PostedFile.FileName);

                        int pos = name.LastIndexOf('.');
                        string ext = name.Substring(pos);

                        data[type] = new Session();

                        data[type].SName = filesent + ext;// name;

                        data[type].SType = contentType;

                        //  data[type].SData = fileData;

                        data[type].SSize = size;
                        data[type].SPath = "~/Documents/" + id + filesent + ext;// name;
                        //


                        //  string savelocation = Server.MapPath("images") + "\\14" + name;
                        fileup.SaveAs(Server.MapPath("~/Documents/") + "\\" + id + filesent + ext);// name);




                        Session["data"] = data;
                        return 1;


                    }
                    catch (Exception exc)
                    {
                        return -1;
                    }
                }
                return -2;
            }
            else
                return -3;
        }

        protected void MessageAlert(string title, string body)
        {
         ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }


        protected void Butsave_Click(object sender, EventArgs e)
        {
            string idno = "6867876";

            Session[] data1 = Session["data"] as Session[];
            //     files=  Session["file"] as testSession[];
            if (data1[0].SName == null) MessageAlert("Error!!!", "Kindly Upload Profile Picture");
            else if (data1[1].SName == null) MessageAlert("Error!!!","Kindly Upload CV");
            else if (data1[2].SName == null)  MessageAlert("Error!!!", "Kindly Upload Motivation Letter");
            else if (data1[3].SName == null)  MessageAlert("Error!!!", "Kindly Upload Metric");
            else if (data1[4].SName == null)  MessageAlert("Error!!!", "Kindly Upload Certificate");
            else if (data1[5].SName == null)  MessageAlert("Error!!!", "Kindly Upload Other");

            else
            {
                ScanFolder(data1, id);  // perform only once
                //    for (int i = 0; i < data1.Length; i++)
                if (data1[0] != null && data1[1] != null && data1[2] != null)
                {
                    //string name = data1[i].SName;
                    //int size = data1[i].SSize;
                    //string type = data1[i].SType;
                    //string path = data1[i].SPath;

                    if (Butsave.Text == "SAVE")
                    {

                        //Save files to disk

                        //     if (dat.SaveInfo(name, size, type, path))
                        if (dat.SaveInfo(data1[0].SName, idno, data1[0].SType, data1[0].SSize, data1[0].SPath) &&
                            dat.SaveInfo(data1[1].SName, idno, data1[1].SType, data1[1].SSize, data1[1].SPath) &&
                            dat.SaveInfo(data1[2].SName, idno, data1[2].SType, data1[2].SSize, data1[2].SPath) &&
                            dat.SaveInfo(data1[3].SName, idno, data1[3].SType, data1[3].SSize, data1[3].SPath) &&
                             dat.SaveInfo(data1[4].SName, idno, data1[3].SType, data1[3].SSize, data1[4].SPath) &&
                         dat.SaveInfo(data1[4].SName, idno, data1[3].SType, data1[3].SSize, data1[5].SPath))
                        {
                            lblResult.Text = "Files Saved Successfully!";
                            MessageAlert("Success!!!", "Files Saved Successfully");
                        }
                        else
                        {
                            MessageAlert("Error!!!", "Error Uploading Files!!");
                            lblResult.Text = "Error Uploading Files!!";
                        }

                    }
                    else
                    {
                        // if (dat.UpdateInfo(name, size, type, path, id))
                        if (dat.UpdateInfo(Convert.ToInt32(sn1.Text), data1[0].SName, idno, data1[0].SType, data1[0].SSize, data1[0].SPath) &&
                            dat.UpdateInfo(Convert.ToInt32(sn2.Text), data1[1].SName, idno, data1[1].SType, data1[1].SSize, data1[1].SPath) &&
                            dat.UpdateInfo(Convert.ToInt32(sn3.Text), data1[2].SName, idno, data1[2].SType, data1[2].SSize, data1[2].SPath) &&
                            dat.UpdateInfo(Convert.ToInt32(sn4.Text), data1[3].SName, idno, data1[3].SType, data1[3].SSize, data1[3].SPath) &&
                                dat.UpdateInfo(Convert.ToInt32(Label2.Text), data1[4].SName, idno, data1[4].SType, data1[4].SSize, data1[4].SPath) &&
                         dat.UpdateInfo(Convert.ToInt32(Label4.Text), data1[5].SName, idno, data1[5].SType, data1[5].SSize, data1[5].SPath))
                        {
                            lblResult.Text = "Files Updated Successfully!";
                            MessageAlert("Success!!!", "Files Updated Successfully");
                        }
                        else
                        {
                            MessageAlert("Error!!!", "Error Updating Files!!");
                            lblResult.Text = "Error Updating Files!!";
                        }
                    }
                }
                else
                {
                    MessageAlert("Error!!!", "Error Updating Files!!");
                    lblResult.Text = "Error Uploading Files!!";
                }

            }
        
         }

        public bool Download(int type)
        {
            try
            {
                Session[] test = Session["data"] as Session[];
                string contype = test[type].SType;
                string fname = test[type].SName;
                string path = test[type].SPath;

                //check for file existence...befre d folowing code

                Response.ContentType = contype; // "image/jpeg"; "text/plain";"application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename= " + id + "_" + fname);//SAIEE_ApplicationForm2013.pdf");// + fname);// logo.jpg");
                Response.TransmitFile(Server.MapPath(path));//"~/images/" +  +id + fname));//14SAIEE_ApplicationForm2013.pdf"));// + fname));//logo.jpg"));
                Response.End();
                return true;


            }
            catch (Exception ex)
            {
                return false;
            }

        }
              

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            //  testSession[] data2 = Session["data"] as testSession[];
            if (!Download(0))
                lblStats1.Text = "Error Downloding file-";

        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            //  testSession[] data2 = Session["data"] as testSession[];
            if (!Download(1))
                lblStats2.Text = "Error Downloding file-";
        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            //  testSession[] data2 = Session["data"] as testSession[];
            if (!Download(2))
                lblStats3.Text = "Error Downloding file-";

        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            //  testSession[] data2 = Session["data"] as testSession[];
            if (!Download(3))
                lblStats4.Text = "Error Downloding file-";
        }

        protected void Picupload_Click(object sender, EventArgs e)
        {

            string nam= FUpix.ClientID;
            int result = UpLoad(nam, 0, "Pic");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton1.Text = name;
            }
            displayErr(result, name, lblStats1);

        }

        private void displayErr(int result, string nam, Label lbl)
        {
            if (result == -1)
                lbl.Text = "Error Uploading " + nam;
            else if (result == -2)
                lbl.Text = "File Size Greater than 500Kb";
            else if (result == -3)
                lbl.Text = "Please Select a File to Upload";
        }

        protected void Cvupload_Click(object sender, EventArgs e)
        {
            string nam = FUCv.ClientID;
            int result = UpLoad(nam, 1, "CV");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton2.Text = name;
            }
            displayErr(result, name, lblStats1);
        }

        protected void Letterupload_Click(object sender, EventArgs e)
        {
            string nam = FUML.ClientID;
            int result = UpLoad(nam, 2, "Letter");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton3.Text = name;
            }
            displayErr(result, name, lblStats1);
        }

        protected void Matricupload_Click(object sender, EventArgs e)
        {
            string nam = FUTC.ClientID;
            int result = UpLoad(nam, 3, "Matric");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton4.Text = name;
            }
            displayErr(result, name, lblStats1);
        }

        protected void certUpload_Click(object sender, EventArgs e)
        {
            string nam = FUcert.ClientID;
            int result = UpLoad(nam, 4, "Cert");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton5.Text = name;
            }
            displayErr(result, name, lblStats1);
        }

        protected void otherupload_Click(object sender, EventArgs e)
        {
            string nam = FUodas.ClientID;
            int result = UpLoad(nam, 5, "other");
            if (result == 1)
            {
                //  LblStat1.Text = name + " Uploaded Successfully";
                LinkButton6.Text = name;
            }
            displayErr(result, name, lblStats1);
        }

        protected void LinkButton5_Click(object sender, EventArgs e)
        {
            if (!Download(4))
                lblStats4.Text = "Error Downloding file-";
        }

        protected void LinkButton6_Click(object sender, EventArgs e)
        {
            if (!Download(5))
                lblStats4.Text = "Error Downloding file-";
        }

        protected void LinkButton1_Click1(object sender, EventArgs e)
        {
            if (!Download(0))
                lblStats1.Text = "Error Downloding file-";
        }

    }
}
    
