﻿<%@ Page Title="Group Allocation" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true"
    CodeBehind="Group.aspx.cs" Inherits="LMS.TRAIN.Group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="GA">
        <div class="topmarg">
        </div>
        <h4>
            GROUP ALLOCATION</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <div id="gridscroll" >
            <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                DataSourceID="SqlDataSource1" >
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" ReadOnly="True" SortExpression="SN">
                        <ItemStyle Width="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" Width="150px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification No." SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Qualif" HeaderText="Qualification" SortExpression="Qualif">
                        <ItemStyle HorizontalAlign="Center" Width="120px" VerticalAlign="Middle" />
                    </asp:BoundField>
                      <asp:BoundField DataField="Qualif" HeaderText="Previous Exposure"           SortExpression="SN">
                        <ItemStyle Width="125px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
       <asp:TemplateField HeaderText="Group">
                       <ItemTemplate>
                            <asp:Label ID="lblgroup" runat="server" Text='<%# Eval("Qualif") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                        <asp:DropDownList ID="ddlgroup" DataSourceID="DataSource2" runat="server">
                     
                        </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="85px" />
                    </asp:TemplateField>
                  <asp:TemplateField>
            <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true"  />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true"  />
            </ItemTemplate>
            <ItemStyle Width="80px" HorizontalAlign="Center" />
        </asp:TemplateField>   

                </Columns>
                                <AlternatingRowStyle BackColor="#EBFDD4"  />
                <RowStyle BackColor="#BEF8CF" />
                 <PagerStyle CssClass="TextboxShadow" />
               </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
             <asp:SqlDataSource ID="DataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT DISTINCT SN FROM [testing]"></asp:SqlDataSource>
        <div class="clear hrspacer"> </div>
        <div>
        <table cellspacing="6" class="width50">
        <tr><td>COUNT OF :<asp:Label ID="lblprog" runat="server"></asp:Label>=</td></tr>
        </table>
         </div>
        <asp:Panel runat="server" id="Pantrain">
            <table cellspacing="6"  class="width80">
                <tr class="pd_td" align="center">
                    <td>
                        <asp:Button ID="butsave" runat="server" Text="SAVE LIST" />
                    </td>
                    <td>
                        <asp:Button ID="Butsend" runat="server" Text="Send For Approval" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server"  id="Panmgt">
            <table cellspacing="6" class="width80">
                <tr class="pd_td" align="center">
                    <td rowspan="2" valign="top">
                    Comments : <asp:TextBox ID="tbcomm" SkinID="TextArea" runat="server" TextMode="MultiLine" />
                    </td>
                    <td>
                        <asp:Button ID="Butaprve" runat="server" Text="APPROVE" />
                    </td>
                </tr>
                <tr class="pd_td" align="center"> <td>
                        <asp:Button ID="Butdisaprv" runat="server" Text="DISAPPROVE" />
                    </td></tr>
            </table>
        </asp:Panel>
       
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
