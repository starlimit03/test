﻿<%@ Page Title="Register" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Registers.aspx.cs" Inherits="LMS.TRAIN.Registers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Reg">
        <div class="topmarg">
        </div>
        <h4>
            ATTENDANCE REGISTER</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table cellspacing="12" class="width80 leftandright">
                    <tr class="pd_td center">
                        <td>
                            Start Date :
                        </td>
                        <td>
                            <asp:TextBox ID="tbcalfrm" runat="server" Enabled="false"></asp:TextBox>
                            <asp:CalendarExtender ID="tbcal_CalendarExtender" runat="server" Enabled="True" TargetControlID="tbcalfrm"
                                PopupButtonID="imgcalfrm" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                            </asp:CalendarExtender>
                            &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalfrm" height="25px" />
                        </td>
                        <td>
                            End Date :
                        </td>
                        <td>
                            <asp:TextBox ID="tbcalto" runat="server" Enabled="false"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="tbcalto"
                                PopupButtonID="imgcalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                            </asp:CalendarExtender>
                            &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalto" height="25px" />
                        </td>
                    </tr>
                    <tr class="pd_td center">
                        <td >
                            Group :
                        </td>
                        <td  align="left">
                            <asp:DropDownList ID="ddlgrp" DataTextField="ProjName" runat="server" AppendDataBoundItems="True"
                                DataSourceID="SqlDataSource1">
                                <asp:ListItem>---Select---</asp:ListItem>
                            </asp:DropDownList> 
                             <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator2" ControlToValidate="ddlgrp" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                SelectCommand="GetAllProject_SP" ProviderName="System.Data.SqlClient" SelectCommandType="StoredProcedure">
                                       <SelectParameters>
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="pname" Type="String" />
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="type" Type="String" />
                                    </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                     <td colspan="2"><asp:Button ID="butprev" runat="server" OnClick="butprev_Click" Text="PREVIEW" /> </td>
                    </tr>
                    <tr class="pd_td">
                        <td colspan="4">
                        <hr class="myhr" />
                        </td>
                    </tr>
                 <%--    <tr class="pd_td">
                        <td colspan="4">
                    
                        </td>
                    </tr>--%>
                </table>
                <div class="table-grid-space"></div>
                <asp:Panel ID="Panprint" runat="server">
                    <div id="gridscroll">
                        <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server">
                            <%--  DataSourceID="SqlDataSource2" OnRowDataBound="GridView1_RowDataBound">--%>
                            <Columns>
                                <asp:TemplateField HeaderText="SN" SortExpression="SN">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsn" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="25px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                                    NullDisplayText="&quot;No data&quot;">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ID_Num" HeaderText="Identification No." SortExpression="IdNo">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CellNum" HeaderText="CellPhone Number" NullDisplayText="&quot;No data&quot;">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProjName" HeaderText="Group">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderStyle-Width="100px" >
                                <HeaderTemplate > <asp:Label ID="lblhd1" runat="server" Text="Day1"></asp:Label></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="100px">
                                <HeaderTemplate> <asp:Label ID="lblhd2" runat="server" Text="Day2"></asp:Label></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="100px" >
                                <HeaderTemplate> <asp:Label ID="lblhd3" runat="server" Text="Day3"></asp:Label></HeaderTemplate>
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderStyle-Width="100px" >
                                <HeaderTemplate> <asp:Label ID="lblhd4" runat="server" Text="Day4"></asp:Label></HeaderTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderStyle-Width="100px">
                                <HeaderTemplate> <asp:Label ID="lblhd5" runat="server" Text="Day5"></asp:Label></HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No. Of Days Present">
                                    <ItemStyle Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle BackColor="#EBFDD4" />
                            <RowStyle BackColor="#BEF8CF" />
                            <PagerStyle CssClass="TextboxShadow" />
                        </asp:GridView>
                    </div>
                    <div>
                    <p class="hrspacer"></p>
                    <hr class="myhr" />
                    <p class="center"><asp:Button ID="butprint" runat="server" Text="PRINT" /> </p>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class=" bottommarg">
           </div>
    </div>
</asp:Content>
