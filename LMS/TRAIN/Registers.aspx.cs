﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer;
using System.Data;

namespace LMS.TRAIN
{
    public partial class Registers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Panprint.Visible = false;
                //  panupdate.Visible = false;
                tbcalfrm.Text = DateTime.Now.AddMonths(-1).ToShortDateString();
                tbcalto.Text = DateTime.Today.ToShortDateString();
                CalendarExtender1.EndDate =  DateTime.Today.AddDays(7);
                tbcal_CalendarExtender.EndDate = DateTime.Today.AddDays(7);
            }
        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void butprev_Click(object sender, EventArgs e)
        {
            string grp;
            DateTime frm, to;// = tbcalto.Text;

            to = Convert.ToDateTime(tbcalto.Text);
            frm = Convert.ToDateTime(tbcalfrm.Text);
            //int wk = 0, days = 0;
            //string RegId = "", id = "";
            
            if (frm.CompareTo(to) == 1)
            {
                MessageAlert("Error", "End-Date cannot preceed Start-Date!!!");
                Panprint.Visible = false; return;
            }

          string [] grp1 = ddlgrp.SelectedItem.Text.Split('{');
          grp = grp1[0].Substring(0, grp1[0].Length);
            RegisterList rl = new RegisterList();
            DataSet ds = rl.GetRegister(grp,"","show","",0,0,"");

            if (ds != null)
            {
                if (ds.Tables[0].Columns.Count > 0)
                {
                    // Populate Gridview with Info;
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    Panprint.Visible = true;
                    //set header details
                    Label lb1 = GridView1.HeaderRow.FindControl("lblhd1") as Label;
                    Label lb2 = GridView1.HeaderRow.FindControl("lblhd2") as Label;
                    Label lb3 = GridView1.HeaderRow.FindControl("lblhd3") as Label;
                    Label lb4 = GridView1.HeaderRow.FindControl("lblhd4") as Label;
                    Label lb5 = GridView1.HeaderRow.FindControl("lblhd5") as Label;

                    lb1.Text = frm.Day.ToString() + "/" + frm.Month.ToString();
                    lb2.Text = frm.AddDays(1).Day.ToString() + "/" + frm.AddDays(1).Month.ToString();
                    lb3.Text = frm.AddDays(2).Day.ToString() + "/" + frm.AddDays(2).Month.ToString();
                    lb4.Text = frm.AddDays(3).Day.ToString() + "/" + frm.AddDays(3).Month.ToString();
                    lb5.Text = frm.AddDays(4).Day.ToString() + "/" + frm.AddDays(4).Month.ToString();
                }
                else
                {
                    MessageAlert("ERROR", "No Data for selected Search!!!");
                    Panprint.Visible = false;
                }
            }
            else
            {
                MessageAlert("ERROR", "No Data for selected Search!!!");
                Panprint.Visible = false;
            }
        }

    

    }
}