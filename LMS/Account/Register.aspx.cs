﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LMS.Account
{
    public partial class Register : System.Web.UI.Page
    {
        static string desturl = "";
        protected void Page_Load(object sender, EventArgs e)
        {
        //  RegisterUser.ContinueDestinationPageUrl
            desturl  = Request.QueryString["ReturnUrl"];
             
           
        }
        public void RegisterUser_CreatedUser(object sender, EventArgs e)
        {

            MembershipCreateStatus p = MembershipCreateStatus.Success;
            Membership.CreateUser(CreateUserWizard1.UserName,
               CreateUserWizard1.Password, CreateUserWizard1.Email,
            CreateUserWizard1.Question, CreateUserWizard1.Answer, true, out p);
        }
      
        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            
            MembershipCreateStatus p = MembershipCreateStatus.Success;
            Membership.CreateUser(CreateUserWizard1.UserName,
               CreateUserWizard1.Password, CreateUserWizard1.Email,
            CreateUserWizard1.Question, CreateUserWizard1.Answer, true, out p);
            if (String.IsNullOrEmpty(desturl))
            {
                desturl = "~/Membershiproles/CreateUserRole.aspx";
            }
            
            Response.Redirect(desturl);
        }
        

        //protected void CreateUserWizard1_ContinueButtonClick(object sender, EventArgs e)
        //{
        //    Response.Redirect("~/Membershiproles/CreateUserRole.aspx");
               
        //}

        protected void ContinueButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Membershiproles/CreateUserRole.aspx");
        }

    }
}
