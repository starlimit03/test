﻿<%@ Page Title="Payroll" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Payroll.aspx.cs" Inherits="LMS.FIN.Payroll" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PR">
        <div class="topmarg">
        </div>
        <h4>
            PAYROLL INFORMATION</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <asp:UpdatePanel ID="UPall" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="panselect" runat="server">
                    <table cellspacing="6" class="width80 leftandright">
                        <tr class="pd_td center">
                            <td>
                                Start Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalfrm" runat="server" Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender ID="tbcal_CalendarExtender" runat="server" Enabled="True" TargetControlID="tbcalfrm"
                                    PopupButtonID="imgcalfrm" FirstDayOfWeek="Monday" PopupPosition="TopRight" Format="MM/dd/yyyy">
                                </asp:CalendarExtender>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalfrm" height="25px" />
                            </td>
                            <td>
                                End Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalto" runat="server" Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="tbcalto"
                                    PopupButtonID="imgcalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                                &nbsp;
                                <img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalto" height="25px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr class="pd_td center">
                            <td>
                                Group :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlgrp" DataTextField="ProjName" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="SqlDataSource2">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator2"
                                    ControlToValidate="ddlgrp" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                    SelectCommand="GetAllProject_SP" ProviderName="System.Data.SqlClient" SelectCommandType="StoredProcedure">
                                </asp:SqlDataSource>
                            </td>
                            <td colspan="2">
                                <asp:Button ID="butpop" runat="server" SkinID="Butlarge" Text="GENERATE PAYROLL" Width="200px" OnClick="butpop_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="myhr1" />
                            </td>
                        </tr>
                    </table>
                    <div class="table-grid-space">
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="panall" Visible="false">
                <asp:Panel runat="server" ID="pangrid" CssClass="gridscroll2">
                    <div id="gridscroll" runat="server">
                        <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"  OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                             <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CbAll" AutoPostBack="true" OnCheckedChanged="SN_ChangedClicked"
                                            Text="SN" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CbRow" AutoPostBack="true" OnCheckedChanged="CbRow_Checked" runat="server" />
                                        <asp:Label ID="lblsn" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                                    NullDisplayText="&quot;No data&quot;">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Identification No." SortExpression="IdNo">                             
                                    <ItemTemplate>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("ID_Num") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="ProjName" HeaderText="Group">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DaysAttended" HeaderText="No Of Days Attended">
                                    <ItemStyle HorizontalAlign="Center" Width="70px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="RegWeek" HeaderText="Week">
                                    <ItemStyle HorizontalAlign="Center" Width="50px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="StipendAmt" HeaderText="Stipend Amount (R)">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="StipendAmt" HeaderText="TextBook Balance (R)">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField >
                                <HeaderTemplate >
                                <asp:Label ID="lblhpaystat" runat="server" Text='Stipend Status'></asp:Label>
                               <asp:DropDownList ID="ddlhpaystat" OnSelectedIndexChanged="ddlhpaystat_changed" AutoPostBack="true" SkinID="GridDdl"  Visible="false" runat="server">
                                            <asp:ListItem>---SELECT---</asp:ListItem>
                                            <asp:ListItem>NOT-PAID</asp:ListItem>
                                            <asp:ListItem>PAID</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlhpaystat"
                                            SetFocusOnError="true" InitialValue="---SELECT---" runat="server" ErrorMessage="*"> </asp:RequiredFieldValidator>
                                         
                                </HeaderTemplate>
                                   <ItemTemplate>                                     
                                    <asp:Label ID="lblpaystat" runat="server" Text='<%# Eval("PaymentStat") %>'></asp:Label>
                                  <asp:DropDownList ID="ddlpaystat" SkinID="GridDdl" SelectedValue='<%# Eval("PaymentStat") %>' Visible="false" runat="server">
                                            <asp:ListItem>---SELECT---</asp:ListItem>
                                            <asp:ListItem>NOT-PAID</asp:ListItem>
                                            <asp:ListItem>PAID</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlpaystat"
                                            SetFocusOnError="true" InitialValue="---SELECT---" runat="server" ErrorMessage="*"> </asp:RequiredFieldValidator>
                                           </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle BackColor="#EBFDD4" />
                            <RowStyle BackColor="#BEF8CF" />
                            <PagerStyle CssClass="TextboxShadow" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
                    <%--       <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>--%>
             
                <div class="clear hrspacer">
                </div>
                <table class="width80">
               
                           <tr class="center">
                            <td colspan="3">
                           <asp:Label ID="lbl" runat="server" CssClass="msglabel blink" Visible="false"></asp:Label>
                            </td> 
                            <td >
                            <asp:Button ID="butupdate" runat="server" OnClick="butupdate_Click" Visible="false" Text="UPDATE" />
                             </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="myhr" />
                            </td>
                        </tr>
                       <tr class="pd_td msglabel" >
                            <td >
                            <asp:Label ID="lblbenef" runat="server" ></asp:Label>
                            </td>
                             <td colspan="2">
                            <asp:Label ID="lbltotalamt" runat="server" ></asp:Label>
                            </td>
                              <td >
                            <asp:Label ID="lbldeduct" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr class="pd_td center">
                            <td colspan="2">
                                    <asp:Button ID="butsend" runat="server" Text="Send For Approval" />
                            </td>
                            <td colspan="2">
                                <asp:Button ID="butprint" runat="server" Text="PRINT" />
                            </td>
                        </tr>
                           <tr class="pd_td">
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
               
                       </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
