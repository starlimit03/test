﻿<%@ Page Title="Attendance Register" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="LMS.FIN.Register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function CheckForMonday(sender, args) {
        if (sender._selectedDate._dayOfWeek != "Monday") {
            alert("Start day must be a MONDAY!" + sender._selectedDate.value);        
        }
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Reg">
        <div class="topmarg">
        </div>
        <h4>
            ATTENDANCE REGISTER</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <asp:UpdatePanel runat="server" ID="UPall" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="panselect" runat="server">
                    <table cellspacing="6" class="width80 leftandright">
                        <tr class="pd_td center">
                            <td>
                                Start Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalfrm" runat="server" Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender ID="tbcal_CalendarExtender" runat="server" Enabled="True" TargetControlID="tbcalfrm"
                                    PopupButtonID="imgcalfrm" FirstDayOfWeek="Monday" PopupPosition="TopRight" 
                                    Format="MM/dd/yyyy" onclientdateselectionchanged="CheckForMonday">
                                </asp:CalendarExtender>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalfrm" height="25px" />
                            </td>
                            <td>
                                End Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalto" runat="server" Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="tbcalto"
                                    PopupButtonID="imgcalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                                &nbsp;
                                <img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalto" height="25px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr class="pd_td center">
                            <td>
                                Group :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlgrp" DataTextField="ProjName" runat="server" AppendDataBoundItems="True"
                                    DataSourceID="SqlDataSource1">
                                    <asp:ListItem>---Select---</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator2"
                                    ControlToValidate="ddlgrp" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                    SelectCommand="GetAllProject_SP" ProviderName="System.Data.SqlClient" SelectCommandType="StoredProcedure">
                                </asp:SqlDataSource>
                            </td>
                            <td colspan="2">
                                <asp:Button ID="butpop" runat="server" Text="RETRIEVE" OnClick="butret_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="myhr1" />
                            </td>
                        </tr>
                    </table>
                    <div class="table-grid-space">
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pangrid" Visible="false">
                    <div id="gridscroll">
                        <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                            OnRowDataBound="GridView1_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CbAll" AutoPostBack="true" OnCheckedChanged="SN_ChangedClicked"
                                            Text="Select All" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CbRow" AutoPostBack="true" OnCheckedChanged="CbRow_Checked" runat="server" />
                                        <asp:Label ID="lblsn" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                                    NullDisplayText="&quot;No data&quot;">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Identification No." SortExpression="IdNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("ID_Num") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Group">
                                    <ItemTemplate>
                                        <asp:Label ID="lblgrp" runat="server" Text='<%# Bind("ProjName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Week">
                                    <ItemTemplate>
                                        <asp:Label ID="lblwk" runat="server" Text='<%# Bind("RegWeek") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No. of Days Attended">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldays" runat="server" Text='<%# Eval("DaysAttended") %>'></asp:Label>
                                        <asp:TextBox ID="tbdays" Text='<%# Eval("DaysAttended") %>' Columns="7" Visible="false"
                                            SkinID="GridViewTb" runat="server" MaxLength="1"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<=5"
                                            ValidationExpression="^([0-5])$" ControlToValidate="tbdays" Font-Size="X-Small"
                                            ForeColor="Red" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="55px" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comments">
                                    <ItemTemplate>
                                        <asp:Label ID="lblreason" runat="server" Text='<%# Eval("Comment") %>'></asp:Label>
                                        <asp:TextBox ID="tbreason" Columns="7" Visible="false" TextMode="MultiLine" SkinID="GridTxtArea"
                                            runat="server" Text='<%# Eval("Comment") %>' Rows="3"></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                            <AlternatingRowStyle BackColor="#EBFDD4" />
                            <RowStyle BackColor="#BEF8CF" />
                            <PagerStyle CssClass="TextboxShadow" />
                        </asp:GridView>
                    </div>
               
                <div class="clear hrspacer">
                </div>
                    <table class="width80">
                     <tr class="pd_td center">
                            <td colspan="4"><asp:Label ID="lbl" Visible="false" CssClass="msglabel blink" runat="server"></asp:Label></td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="myhr" />
                            </td>
                        </tr>
                     
                        <tr class="pd_td center">
                       <div id="divupdate" runat="server" visible="false">  <td > <asp:Button ID="butprev" runat="server" 
                                    Text="UPDATE" OnClick="butup_Click" />
                           &nbsp;</td> </div>
                        
                         <div id="divverify" runat="server" visible="false">  <td > <asp:Button ID="butvery" runat="server" 
                                    Text="VERIFY" OnClick="butvery_Click" />
                           &nbsp;</td> </div>    
                            <td >
                             <asp:Button ID="butgenpay" runat="server" SkinID="Butlarge" Text="GENERATE PAYROLL" />
                                                            </td>
                            <td >
                                <asp:Button ID="butprint" runat="server" Text="PRINT" />
                            </td>
                        </tr>
                        <tr class="pd_td center">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                           <tr class="pd_td">
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div class="bottommarg">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
