﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer;
using System.Data;

namespace LMS.FIN
{
    public partial class Register : System.Web.UI.Page
    {
        static string regid = null;

        RegisterList rl = new RegisterList();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               pangrid.Visible = false;
               divupdate.Visible = false;
                tbcalfrm.Text = DateTime.Now.AddMonths(-1).ToShortDateString();
                tbcalto.Text = DateTime.Today.ToShortDateString();
                CalendarExtender1.EndDate = DateTime.Today.AddDays(7);
                tbcal_CalendarExtender.EndDate = DateTime.Today.AddDays(7);
            }
        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void butret_Click(object sender, EventArgs e)
        {
            string grp;
            DateTime frm, to;// = tbcalto.Text;

            to = Convert.ToDateTime(tbcalto.Text);
            frm = Convert.ToDateTime(tbcalfrm.Text);
            int wk=0,days=0;
            string RegId="", id="";

            if (frm.CompareTo(to) == 1)
            {
                MessageAlert("Error", "End-Date cannot preceed Start-Date !!!");
                pangrid.Visible = false; return;
            }
          
            wk = to.DayOfYear / 7;
            grp = ddlgrp.SelectedItem.Text;
            RegId = grp + "_" + frm.ToShortDateString() + "_" + to.ToShortDateString();
       DataSet ds= rl.GetRegister(grp,"","get",RegId,wk,days,id);

       if (ds != null)
       {
           if (ds.Tables[0].Columns.Count > 0)
           {
               // Populate Gridview with Info;
               GridView1.DataSource = ds;
               GridView1.DataBind();

             string app=  ds.Tables[0].Rows[0].Field<string>("RegisterStat") ;
             regid = ds.Tables[0].Rows[0].Field<string>("RegisterID");
             if (app == "YTBV")
             { divverify.Visible = true; lbl.Text = "This Register is Pending Verification!!!"; lbl.Visible = true; }
             else { divverify.Visible = false; lbl.Visible = false; }
              
               pangrid.Visible = true;
           }
       }
       else
       {
           MessageAlert("ERROR", "No Data for selected Search!!!");
           pangrid.Visible = false;
       }
        
      }

        protected void tbcalfrm_datechanged(object sender, EventArgs e)
        {

            DateTime frm = tbcal_CalendarExtender.SelectedDate.Value;
            tbcalto.Text = frm.AddDays(7).ToShortDateString();
        }
        protected void SN_ChangedClicked(object sender, EventArgs e)
        {
         //   divupdate.Visible = true;
            CheckBox cbh = GridView1.HeaderRow.FindControl("CbAll") as CheckBox;
            foreach (GridViewRow r in GridView1.Rows)
            {
               
                CheckBox cbc = ((CheckBox)r.FindControl("CbRow"));
                Label lbday = ((Label)r.FindControl("lbldays"));
                TextBox tbday = ((TextBox)r.FindControl("tbdays"));
                Label lbrsn = ((Label)r.FindControl("lblreason"));
                TextBox tbrsn = ((TextBox)r.FindControl("tbreason"));
                
              
                if (cbh.Checked)
                {
                    cbc.Checked = true; lbday.Visible = false; tbday.Visible = true; lbrsn.Visible = false; tbrsn.Visible = true;
                }
                else
                {
                    cbc.Checked = false; lbday.Visible = true; tbday.Visible = false; lbrsn.Visible = true; tbrsn.Visible = false;
                }
            }
            checkrows();  
        }

        private void checkrows()   // Check if any row is checked for update
        {
            int i = 0; divupdate.Visible = false;
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("CbRow"));
                if (cbc.Checked) { i++; divupdate.Visible = true; break; }
            }
        }

        protected void CbRow_Checked(object sender, EventArgs e)
        {

          //  divupdate.Visible = true;
            CheckBox cb = (CheckBox)sender;
            GridViewRow r = (GridViewRow)cb.NamingContainer;

            Label lbday = ((Label)r.FindControl("lbldays"));
            TextBox tbday = ((TextBox)r.FindControl("tbdays"));
            Label lbrsn = ((Label)r.FindControl("lblreason"));
            TextBox tbrsn = ((TextBox)r.FindControl("tbreason"));

            if (cb.Checked)
            {
                lbday.Visible = false;
                tbday.Visible = true;
                lbrsn.Visible = false;
                tbrsn.Visible = true;
                //   panupdate.Visible = true;
            }
            else
            {
                lbday.Visible = true;
                tbday.Visible = false;
                lbrsn.Visible = true;
                tbrsn.Visible = false;
                //   panupdate.Visible = false;
            }
            checkrows();
        }

        protected void butup_Click(object sender, EventArgs e)
        {
            int i = 0;
            origdatatable = (DataTable)ViewState["originalvaluesdatatable"];
           RegisterList lst = new RegisterList(); // to update the database based on stored procedures
          string frm, to;// = tbcalto.Text;

           to = Convert.ToDateTime(tbcalto.Text).ToShortDateString();
           frm = Convert.ToDateTime(tbcalfrm.Text).ToShortDateString();
      //     int wk=  Convert.ToDateTime(tbcalfrm.Text).DayOfYear / 7;
          
           
            foreach (GridViewRow r in GridView1.Rows)
                if (IsModified(r))
                {

                    string regid = group + "_" + frm + "_" + to;
                    if (lst.UpdateRegister(group,reason,"save",regid,week,days,id)) // week not updated
                        i++;
                }
            if (i > 0)
            {
                MessageAlert("Info", i + " Rows Successfully Updated!");

            }
            else MessageAlert("Info", "No Row(s) Updated!");
            tablecopied = false;
            butret_Click(null, null);
            divupdate.Visible = false;
        }
       
        private bool tablecopied = false;
        private DataTable origdatatable;
        private string id, reason,group;
        private int days,week;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                if (!tablecopied)
                {
                    origdatatable = ((DataRowView)e.Row.DataItem).Row.Table.Copy();
                    ViewState["originalvaluesdatatable"] = origdatatable;
                    tablecopied = true;
                }
        }

        protected bool IsModified(GridViewRow r)
        {
            int day,wk;
            string  rsn , idnum,grp;

            day = Convert.ToInt16(((TextBox)r.FindControl("tbdays")).Text);
            rsn = ((TextBox)r.FindControl("tbreason")).Text;

            CheckBox cb = ((CheckBox)r.FindControl("CbRow"));

            idnum = ((Label)r.FindControl("lblid")).Text;
            grp = ((Label)r.FindControl("lblgrp")).Text;
            wk = Convert.ToInt16(((Label)r.FindControl("lblwk")).Text);

            id = idnum; days = day; reason = rsn; group = grp; week = wk;

            DataRow row = origdatatable.Select(string.Format("ID_Num = '{0}'", idnum))[0];

            if (!day.Equals(Convert.ToInt16(row["DaysAttended"]))) { return true; }
            if (!rsn.Equals(row["Comment"].ToString())) { return true; }

             return false;
        }

        protected void tbcalfrm_TextChanged(object sender, EventArgs e)
        {
            tbcalto.Text = CalendarExtender1.SelectedDate.Value.AddDays(7).ToShortDateString();
        }

        protected void butvery_Click(object sender, EventArgs e)
        {

            if (rl.UpdateRegister("", "", "approve", regid, 0, 0, "")) // just approve all ID which belong to the Register ID;
            {
                MessageAlert("Success", "Register Successfully Approved!");
                lbl.Visible = false;
                divverify.Visible = false;
              //  butret_Click(null, null);
            }
            else
            {
                MessageAlert("Error", "Error Approving Register!");
            }
        }

     

     

    }
}