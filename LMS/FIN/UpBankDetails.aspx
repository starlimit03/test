﻿<%@ Page Title="Update Bank Details" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true" CodeBehind="UpBankDetails.aspx.cs" Inherits="LMS.FIN.UpBankDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="UBD">
<div class="topmarg">
        </div>
        <h4>
          UPDATE BANK DETAILS</h4>
        <hr class="myhr" />
        <div class="hrspacer pd_td">
        </div>
        <div id="bankup">
        <asp:Panel runat="server" ID="Pansearch">
        <table cellspacing="6" class="width80 largtxt" ><tr class="pd_td"><td>Group :  
            <asp:DropDownList ID="DropDownList1" runat="server" 
                DataSourceID="SqlDataSource2" DataTextField="ProgName" 
                DataValueField="ProgName">
            </asp:DropDownList> 
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="Data Source=STARLIMIT-PC\SQLEXPRESS;Initial Catalog=LMS_DB;Integrated Security=True" 
                ProviderName="System.Data.SqlClient" SelectCommand="GetAllProject_SP" 
                SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </td>
        <td>Identification No. :  <asp:TextBox ID="tbid" runat="server"></asp:TextBox></td>
        </tr>
        <tr class="pd_td"><td colspan="2" align="center"><asp:Button ID="butser" runat="server" Text="RETRIEVE" /></td></tr>
        <tr class="pd_td"><td></td></tr>
        </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="Panform">
        <table cellspacing="6" class="width50 largtxt">
        <tr class="pd_td">
        <td colspan="2">Name : <asp:Label ID="lblnam" runat="server"></asp:Label></td>
        </tr>
        <tr class="pd_td">
        <td>Bank Name :</td><td> <asp:TextBox runat="server" ID="tbbnknam" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Bank Account Number :</td><td> <asp:TextBox runat="server" ID="tbantnum" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Account Holder :</td><td> <asp:TextBox runat="server" ID="tbacnthold" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Account Type :</td><td> <asp:TextBox runat="server" ID="tbacnttyp" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Branch :</td><td> <asp:TextBox runat="server" ID="tbbranch" ></asp:TextBox></td>
        </tr>
        <tr class="pd_td"><td>Bank Update Form :</td>
        <td>
            <asp:FileUpload ID="FUform" runat="server" /></td></tr>
             <tr class="pd_td"><td></td></tr>
        <tr align="center"><td colspan="2"><asp:Button ID="butup" runat="server" Text="UPDATE" /></td></tr>
      
        </table>
        </asp:Panel>
        </div>
        <div class="bottommarg">
        </div>
</div>
</asp:Content>
