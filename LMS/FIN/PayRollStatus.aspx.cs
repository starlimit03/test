﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LMS.FIN
{
    public partial class PayRollStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cbsponsor_CheckedChanged(object sender, EventArgs e)
        {
            if(cbsponsor.Checked)
            divspons.Visible = true;
            else
             divspons.Visible = false;
        }

        protected void cbprog_CheckedChanged(object sender, EventArgs e)
        {
            if (cbprog.Checked)
                divprog.Visible = true;
            else
                divprog.Visible = false;
        }

        protected void cbproj_CheckedChanged(object sender, EventArgs e)
        {
            if (cbproj.Checked)
                divproj.Visible = true;
            else
                divproj.Visible = false;
        }
    }
}