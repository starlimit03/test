﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using BusinessLogicLayer;
using System.Data;

namespace LMS.FIN
{
    public partial class Payroll : System.Web.UI.Page
    {
        RegisterList lst = new RegisterList();
        PayRollInfo pay = new PayRollInfo();
        static string reg = null;
        static string pays = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                panall.Visible = false;
                tbcalfrm.Text = DateTime.Now.AddMonths(-1).ToShortDateString();
                tbcalto.Text = DateTime.Today.ToShortDateString();
            }
        }
        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void butpop_Click(object sender, EventArgs e)
        {

            string grp;
            DateTime frm, to;// = tbcalto.Text;

            to = Convert.ToDateTime(tbcalto.Text);
            frm = Convert.ToDateTime(tbcalfrm.Text);
            int wk = 0, days = 0;
            string RegId = "",PayId, id = "";

            if (frm.CompareTo(to) == 1)
            {
                MessageAlert("Error", "End-Date cannot preceed Start-Date !!!");
                panall.Visible = false; return;
            }

            wk = to.DayOfYear / 7;
            grp = ddlgrp.SelectedItem.Text;
            RegId = grp + "_" + frm.ToShortDateString() + "_" + to.ToShortDateString();
            // check if Register ID exists... if not show error message
            DataSet ds = lst.GetRegister(grp, "", "validate", RegId, wk, days, id);
                       
          if(ds==null)
            {
                MessageAlert("Error","No Register Information for Selected Criteria");
                panall.Visible = false;
                return;
            }
               //RegID exists
           
               PayId = "Pay_" + RegId;   // Pay_C-Sharp_12/3/2014_10/7/2014
               // Populate PayRoll Infomation...Create if it doesnt exist.

               ds = null;
              ds= pay.GetPayroll(PayId, RegId);
            if(ds==null)
            {
                MessageAlert("Error", "Error Generating PayRoll!!!");
                panall.Visible = false;
                return;
            }

            GridView1.DataSource = ds;
            GridView1.DataBind();
            panall.Visible = true;

            string app = ds.Tables[0].Rows[0].Field<string>("RegisterStat");
            reg = ds.Tables[0].Rows[0].Field<string>("RegisterID");
            pays = "Pay_"+reg;

            if (app == "YTBV")
            {  lbl.Text = "This Register is Pending Verification, hence cannot be sent for Approval!!!"; lbl.Visible = true;
            pangrid.Enabled = false;
            butsend.Enabled = false; butsend.BackColor = Color.Gainsboro;                
            }
            else
            {
                
                pangrid.Enabled = true;
                lbl.Visible = false;
                butsend.Enabled = true; butsend.BackColor = Color.FromName("#339966");  // color of buttons
            }

            // get the count of beneficiaries and the sum of stipend payment for the selected payroll
            ds = null;
            ds = pay.GetRegisterStat(reg);

            if (ds != null)
            {
                if (ds.Tables[0].Columns.Count > 0)
                {
                    
                    lblbenef.Text ="Total Nummber of beneficiaries =:<b> "+ ds.Tables[0].Rows[0].Field<int>("Benef").ToString()+"</b>";
                    lbltotalamt.Text = "Total Stipend Amount =: <b>R " + ds.Tables[0].Rows[0].Field<decimal>("StipendSum").ToString() + "</b>";
                    lblbenef.Visible = true;
                    lbltotalamt.Visible = true;
                }
            }
            else
            {
                lblbenef.Visible = false;
                lbltotalamt.Visible = false;
            }
           
        }

        private bool tablecopied = false;
        private DataTable origdatatable;
        private string id,stat;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                if (!tablecopied)
                {
                    origdatatable = ((DataRowView)e.Row.DataItem).Row.Table.Copy();
                    ViewState["originalvaluesdatatable"] = origdatatable;
                    tablecopied = true;
                }
        }

        protected void butupdate_Click(object sender, EventArgs e)
        {
            int i = 0;
            origdatatable = (DataTable)ViewState["originalvaluesdatatable"];
            PayRollInfo payinf = new PayRollInfo();
         
            foreach (GridViewRow r in GridView1.Rows)
                if (IsModified(r))
                {
                    if (payinf.UpdatePayroll(id, reg, pays,stat))
                        i++;
                }
            if (i > 0)
            {
                MessageAlert("Info", i + " Rows Successfully Updated!");

            }
            else MessageAlert("Info", "No Row(s) Updated!");
            tablecopied = false;
            butupdate.Visible = false;
            butpop_Click(null, null);
        }

        protected bool IsModified(GridViewRow r)
        {
           
            string status, idnum;

            status = ((DropDownList)r.FindControl("ddlpaystat")).SelectedItem.Text;

            CheckBox cb = ((CheckBox)r.FindControl("CbRow"));

            idnum = ((Label)r.FindControl("lblid")).Text;

            id = idnum;
            stat = status;
            DataRow row = origdatatable.Select(string.Format("ID_Num = '{0}'", idnum))[0];

            if (!status.Equals(row["PaymentStat"].ToString())) { return true; }
            return false;
        }



        protected void SN_ChangedClicked(object sender, EventArgs e)
        {
            CheckBox cbh = GridView1.HeaderRow.FindControl("CbAll") as CheckBox;
            DropDownList ddlh = GridView1.HeaderRow.FindControl("ddlhpaystat") as DropDownList;
            Label lblh = GridView1.HeaderRow.FindControl("lblhpaystat") as Label;

            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("CbRow"));
                Label lbstat = ((Label)r.FindControl("lblpaystat"));
                DropDownList ddlstat = ((DropDownList)r.FindControl("ddlpaystat"));
                
                if (cbh.Checked)
                {
                    cbc.Checked = true; lbstat.Visible = false; ddlstat.Visible = true; lblh.Visible = false;
                    ddlh.Visible = true; 
                }
                else
                {
                    cbc.Checked = false; lbstat.Visible = true; ddlstat.Visible = false; lblh.Visible = true;
                    ddlh.Visible = false; 
                }
            }
            checkrows();
        }

        protected void CbRow_Checked(object sender, EventArgs e)
        {

            CheckBox cb = (CheckBox)sender;
            GridViewRow r = (GridViewRow)cb.NamingContainer;

            Label lbstat = ((Label)r.FindControl("lblpaystat"));
            DropDownList ddlstat = ((DropDownList)r.FindControl("ddlpaystat"));
            
            if (cb.Checked)
            {
                lbstat.Visible = false;
                ddlstat.Visible = true;
            }
            else
            {
                lbstat.Visible = true;
                ddlstat.Visible = false;
            }
            checkrows();
        }

        private void checkrows()   // Check if any row is checked for update
        {
            butupdate.Visible = false;
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("CbRow"));
                if (cbc.Checked) { butupdate.Visible = true; return; }
            }
        } 

        protected void ddlhpaystat_changed(object sender, EventArgs e)
        {

            DropDownList ddlh = GridView1.HeaderRow.FindControl("ddlhpaystat") as DropDownList;
            foreach (GridViewRow r in GridView1.Rows)
            {
                DropDownList ddlstat = ((DropDownList)r.FindControl("ddlpaystat"));
                ddlstat.SelectedIndex = ddlh.SelectedIndex;
            }
        }
         
    }
}