﻿<%@ Page Title="TextBook Refund" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true" CodeBehind="TBRefund.aspx.cs" Inherits="LMS.FIN.TBRefund" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 260px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="TBR">
        <div class="topmarg">
        </div>
        <h4>
       TEXTBOOK REFUND</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
   
        <div id="gridscroll">
            <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                AutoGenerateColumns="False" CellPadding="2" DataKeyNames="Name" DataSourceID="SqlDataSource1"
                BackColor="Transparent" BorderColor="Transparent" BorderWidth="2px" ForeColor="Black"
                GridLines="None" BorderStyle="Solid" PageSize="4" CellSpacing="3" HorizontalAlign="Center"
                AllowPaging="True">
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" ReadOnly="True" SortExpression="SN">
                        <ItemStyle Width="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name"  NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification No." >
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                         </asp:BoundField>
                          <asp:BoundField DataField="IdNo" HeaderText="Group" >
                        <ItemStyle HorizontalAlign="Center" Width="90px" VerticalAlign="Middle" />
                    </asp:BoundField>
                          <asp:BoundField DataField="IdNo" HeaderText="Textbook Balance (R)" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="90px" VerticalAlign="Middle" />
                    </asp:BoundField>
                      <asp:BoundField DataField="IdNo" HeaderText="TextBook Number" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="90px" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="IdNo" HeaderText="Return Date" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="90px" VerticalAlign="Middle" />
                    </asp:BoundField>
                      <asp:BoundField DataField="IdNo" HeaderText="Textbook Refund Status" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="90px" VerticalAlign="Middle" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle BackColor="Tan" CssClass="fixedhead" Font-Size="Medium" />
                <RowStyle BorderStyle="Solid" BackColor="LightGoldenrodYellow" BorderColor="Green"
                    BorderWidth="2px" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" BorderStyle="Solid" BorderColor="Green"
                    BorderWidth="2px" />
                <PagerStyle BackColor="Transparent" ForeColor="Black" Font-Bold="true" HorizontalAlign="Center" />
                <PagerSettings Mode="NextPreviousFirstLast" FirstPageImageUrl="../images/Styling/arrow_first.png"
                    LastPageImageUrl="../images/Styling/arrow_last.png" NextPageImageUrl="../images/Styling/arrow_right.png"
                    PreviousPageImageUrl="../images/Styling/arrow_left.png" Position="TopAndBottom" />
            </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        <div class="clear hrspacer">
        </div>
             <table cellspacing="6" class="width80">
            <tr class="pd_td largtxt">
                <td class="style1">
                   Textbook Refund Amount (R) :
                </td>
                <td>
                <asp:Label ID="lblrefund" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr class="pd_td">
                <td class="style1">
                </td>
            </tr>
        </table>
        <p style="text-align: center">
            <asp:Button ID="butsend" runat="server" Text="Send Notification" /></p>
        <div class="bottommarg">
        </div>
   

</div>
</asp:Content>
