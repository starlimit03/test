﻿<%@ Page Title="Learners Bank Details" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true"
    CodeBehind="BankDetails.aspx.cs" Inherits="LMS.FIN.BankDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="BD">
        <div class="topmarg">
        </div>
        <h4>
           LEARNERS BANK DETAILS</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <div id="gridscroll">
            <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin:auto" runat="server" 
                AutoGenerateColumns="False" CellPadding="2" DataKeyNames="Name" DataSourceID="SqlDataSource1"
                BackColor="Transparent" BorderColor="Transparent" BorderWidth="2px" ForeColor="Black"
                GridLines="None" BorderStyle="Solid"   CellSpacing="3" HorizontalAlign="Center"
                AllowPaging="True">
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" ReadOnly="True" SortExpression="SN">
                        <ItemStyle Width="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center"  VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification No." SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Qualif" HeaderText="Group" SortExpression="Qualif">
                        <ItemStyle HorizontalAlign="Center" Width="100px"   VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Bank Details Supplied?" SortExpression="IdNo">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:CommandField HeaderText="Update" ShowCancelButton="False" 
                        ShowSelectButton="True">
                    <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                    </asp:CommandField>
                </Columns>
                <HeaderStyle BackColor="Tan" CssClass="fixedhead" Font-Size="Medium" />
                <RowStyle BorderStyle="Solid" BackColor="LightGoldenrodYellow" BorderColor="Green"
                    BorderWidth="2px" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" BorderStyle="Solid" BorderColor="Green"
                    BorderWidth="2px" />
                <PagerStyle  BackColor="Transparent" ForeColor="Black" Font-Bold="true" HorizontalAlign="Center" />
               <PagerSettings    Mode="NextPreviousFirstLast"  FirstPageImageUrl="../images/Styling/arrow_first.png" LastPageImageUrl="../images/Styling/arrow_last.png" NextPageImageUrl="../images/Styling/arrow_right.png" PreviousPageImageUrl="../images/Styling/arrow_left.png" Position="TopAndBottom" />
            </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        <div class="clear hrspacer">
        </div>
        <div id="bankdet" >
        <table cellspacing="6" class="width50 largtxt">
        <tr class="pd_td">
        <td>Name : <asp:Label ID="lblnam" runat="server"></asp:Label></td>
        </tr>
        <tr class="pd_td">
        <td>Bank Name :</td><td> <asp:TextBox runat="server" ID="tbbnknam" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Bank Account Number :</td><td> <asp:TextBox runat="server" ID="tbantnum" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Account Holder :</td><td> <asp:TextBox runat="server" ID="tbacnthold" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Account Type :</td><td> <asp:TextBox runat="server" ID="tbacnttyp" ></asp:TextBox></td>
        </tr>
          <tr class="pd_td">
        <td>Branch :</td><td> <asp:TextBox runat="server" ID="tbbranch" ></asp:TextBox></td>
        </tr>
        <tr class="pd_td"><td></td></tr>
        <tr align="center"><td colspan="2"><asp:Button ID="butsav" runat="server" Text="SAVE" /></td></tr>
        </table>
        </div>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
