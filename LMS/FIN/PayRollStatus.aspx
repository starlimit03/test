﻿<%@ Page Title="PayRoll Status" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true" CodeBehind="PayRollStatus.aspx.cs" Inherits="LMS.FIN.PayRollStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PRS">
  <div class="topmarg">
        </div>
        <h4>
          PAYROLL STATUS INFORMATION</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <asp:UpdatePanel runat="server" ID="upall" UpdateMode="Always">
        <ContentTemplate>
        <asp:Panel ID="panselect" runat="server">
        <table class="width50 leftandright" cellspacing="10" >
        <tr class="pd_td">
        <td>
        <asp:CheckBox Font-Bold="true" ID="cbsponsor" runat="server" AutoPostBack="true" 
                Text="&nbsp;&nbsp;&nbsp;Populate By Sponsors" oncheckedchanged="cbsponsor_CheckedChanged" />
        </td>
       <div runat="server" id="divspons" visible="false">
        <td>Sponsors :</td>
        <td> <asp:DropDownList ID="ddlspons" runat="server" AppendDataBoundItems="True" 
                DataSourceID="SqlDataSource2" DataTextField="CompName" 
                DataValueField="CompName" >
        <asp:ListItem>---Select---</asp:ListItem>
        </asp:DropDownList>  
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                SelectCommand="GetAllCompany" SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
         <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator2" ControlToValidate="ddlspons" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>     </td>
      </div>
        </tr>
         <tr class="pd_td">
        <td>
        <asp:CheckBox Font-Bold="true" ID="cbprog" runat="server" AutoPostBack="true" 
                Text="&nbsp;&nbsp;&nbsp;Populate By Program" oncheckedchanged="cbprog_CheckedChanged" />
        </td>
    <div runat="server" id="divprog" visible="false">
        <td>Programs :</td><td> <asp:DropDownList ID="ddlprog" runat="server" 
            AppendDataBoundItems="True" DataSourceID="SqlDataSource3" 
            DataTextField="ProgName" DataValueField="ProgName" >
        <asp:ListItem>---Select---</asp:ListItem>
        </asp:DropDownList>  
         <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
            SelectCommand="GetAllProgram_SP" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
         <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator1" ControlToValidate="ddlprog" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
   </div>
        </tr>
         <tr class="pd_td">
        <td>
        <asp:CheckBox Font-Bold="true" ID="cbproj" runat="server" AutoPostBack="true" 
                Text="&nbsp;&nbsp;&nbsp;Populate By Project" oncheckedchanged="cbproj_CheckedChanged" />
        </td>
   <div runat="server" id="divproj" visible="false">
        <td>Projects :</td><td> <asp:DropDownList ID="ddlproj" runat="server" 
            AppendDataBoundItems="True" DataSourceID="SqlDataSource4" 
            DataTextField="ProjName" DataValueField="ProjName" >
        <asp:ListItem>---Select---</asp:ListItem>
        </asp:DropDownList> 
         <asp:SqlDataSource ID="SqlDataSource4" runat="server" 
            ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
            SelectCommand="GetAllProject_SP" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
         <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator3" ControlToValidate="ddlproj" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator> 
        </td>
        </div>
        </tr>

        <tr class="pd_td">
        <td colspan="3">
        <hr class="hrspacer myhr" />
       </td>
     
        </tr>  
         <tr class="pd_td center">
        <td colspan="3">
       <asp:Button ID="butpop" runat="server" Text="POPULATE" />
       </td>
     
        </tr>
         <%-- <tr class="pd_td">
        <td colspan="3">
        <hr class="hrspacer" />
       </td>
     
        </tr>--%> 
        </table>
          <div class="table-grid-space"></div>
        </asp:Panel>
        <asp:Panel ID="pangrid" runat="server">
        <div id="gridscroll">
         <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server">
                            <%--  DataSourceID="SqlDataSource2" OnRowDataBound="GridView1_RowDataBound">--%>
           <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" ReadOnly="True" SortExpression="SN">
                        <ItemStyle Width="25px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Payroll ID" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Program" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
   <asp:BoundField DataField="IdNo" ReadOnly="true" HeaderText="Project" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Qualif" HeaderText="Week" SortExpression="Qualif">
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Payroll Status">
                       <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Qualif") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                        <asp:DropDownList runat="server">
                        <asp:ListItem>PAID</asp:ListItem>
                        <asp:ListItem>NOT-PAID</asp:ListItem>
                        </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                    </asp:TemplateField>
                      <asp:BoundField DataField="Qualif" HeaderText="Amount Paid (R)" >
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:CommandField ButtonType="Button"  HeaderText="EDIT" ShowEditButton="True" />
                </Columns>
                 <AlternatingRowStyle BackColor="#EBFDD4" />
                            <RowStyle BackColor="#BEF8CF" />
                            <PagerStyle CssClass="TextboxShadow" />
                        </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        </asp:Panel>
        <div class="clear hrspacer">
        </div>
        <p style="text-align:center">
            <asp:Button ID="butsend" runat="server" Text="Send For Approval" /></p>
      
               </ContentTemplate>
        </asp:UpdatePanel>
                <div class="bottommarg">
        </div>
</div>
</asp:Content>
