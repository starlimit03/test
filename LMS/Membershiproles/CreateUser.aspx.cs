﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace LMS.Membershiproles
{
    public partial class CreateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CreateUserWizard1.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        }
        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            FormsAuthentication.SetAuthCookie(CreateUserWizard1.UserName, false /* createPersistentCookie */);

            string continueUrl = CreateUserWizard1.ContinueDestinationPageUrl;
            if (String.IsNullOrEmpty(continueUrl))
            {
                continueUrl = "~/";
            }
            
            Response.Redirect("~/Membershiproles/CreateUserRole.aspx");
        }

        protected void ContinueButton_Click(object sender, EventArgs e)
        {
            Session["User"] = CreateUserWizard1.UserName;
            Response.Redirect("~/Membershiproles/CreateUserRole.aspx");
           
        }
    }
}