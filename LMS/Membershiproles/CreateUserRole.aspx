﻿<%@ Page Title="" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true"
    CodeBehind="CreateUserRole.aspx.cs" Inherits="LMS.MembershipRoles.CreateUserRole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            text-align: right;
        }
          .center
        {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="topmarg">
    </div>
    <center>
        <h4 style="font-weight: bold; text-decoration: underline">
            User Roles</h4>
    </center>
    <table class="width80">
        <tr class="pd_td">
            <td>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Label ID="lbluser" runat="server" Text="Role :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtusername" runat="server">
                </asp:TextBox>
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Create Role" OnClick="btnassign_Click" />
            </td>
        </tr>
        <tr>
            <td class="style1">
                New User :
            </td>
            <td>
                <asp:TextBox ID="txtNewUser" runat="server"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="pd_td center">
            <td colspan="3">
            <asp:Label ID="Label1" ForeColor="Red" runat="server"></asp:Label>
            </td>

        </tr>
        <tr align="center">
            <td>
                <h5>
                    Availble Roles
                </h5>
            </td>
            <td>
                <h5>
                    Availble Users
                </h5>
            </td>
            <td>
            </td>
        </tr>
        <tr class="center">
            <td>
                <asp:ListBox ID="Listboxrole" runat="server" ></asp:ListBox>
            </td>
            <td>
                <asp:ListBox ID="Listboxuser" runat="server"  SelectionMode="Multiple">
                </asp:ListBox>
            </td>
            <td>
            </td>
        </tr>
        <tr class="pd_td"><td></td></tr>
        <tr class="center">
            <td>
                <asp:Button ID="btnassignrole" runat="server" Text="Assign Role" OnClick="btnassignrole_Click" />
            </td>
            <td >
                <asp:Button ID="Btndelete" runat="server" Text="Remove User Role" OnClick="Btndelete_Click" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="color: Red; text-align: center">
                <asp:Label ID="lbldisplay" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class=" pd_td">
            <td>
            </td>
        </tr>
    </table>
    <div class="bottommarg"></div>
</asp:Content>
