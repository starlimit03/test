﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;
using System.Configuration;


namespace LMS.MembershipRoles
{

    public partial class CreateUserRole : System.Web.UI.Page
    {
        string cnn = ConfigurationManager.ConnectionStrings["testingConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
           // txtNewUser.Text = Session["User"].ToString();
            if (!Page.IsPostBack)
            {
                BindUsers();
                BindRoles();
            }

        }

        public void BindRoles()
            {
           
                SqlDataAdapter da = new SqlDataAdapter("select RoleName from aspnet_Roles", cnn);
                DataSet ds = new DataSet();
                da.Fill(ds, "Roles");
                Listboxrole.DataSource = null;
                 Listboxrole.DataSource = ds;
                 Listboxrole.DataTextField = "RoleName";
                 Listboxrole.DataValueField = "RoleName";
                 Listboxrole.DataBind();
            }
        public void BindUsers()
            {
                SqlDataAdapter da = new SqlDataAdapter("select UserName from aspnet_users", cnn);
                DataSet ds = new DataSet();
                da.Fill(ds, "Users");
                Listboxrole.DataSource = null;
                Listboxuser.DataSource = ds;
                 Listboxuser.DataTextField = "UserName";
                Listboxrole.DataValueField = "UserName";
                 Listboxuser.DataBind();
            }
        protected void btnassign_Click(object sender, EventArgs e)
        {
               if (!Roles.RoleExists(txtusername.Text))
                {
                    Roles.CreateRole(txtusername.Text);
                    BindUsers();
                    BindRoles();
                    lbldisplay.Text= "Role(s) Created Successfully";
                }
                else
                {
                    lbldisplay.Text = "Role(s) Already Exists";
                }
   
          
         }

        protected void btnassignrole_Click(object sender, EventArgs e)
        {
            if (txtNewUser.Text != string.Empty)
            {

                if (!Roles.IsUserInRole(Listboxrole.SelectedItem.Text))
                {

                    Roles.AddUserToRole(txtNewUser.Text,
                    Listboxrole.SelectedItem.Text);
                    BindUsers();
                    BindRoles();
                    Label1.Text = "User: "+txtNewUser.Text+"  Assigned to role: "+Listboxrole.SelectedItem.Text+" Successfully";
                }
                else
                {
                    Label1.Text = "Role(s) Already Assigned To User";
                }
            }
            else
                if (!Roles.IsUserInRole(Listboxrole.SelectedItem.Text))
                {

                    Roles.AddUserToRole(Listboxuser.SelectedItem.Text,
                    Listboxrole.SelectedItem.Text);
                    BindUsers();
                    BindRoles();
                    Label1.Text = "User: " + Listboxuser.SelectedItem.Text + "  Assigned to role: " + Listboxrole.SelectedItem.Text + " Successfully";
                }
                else
                {
                    Label1.Text = "Role(s) Already Assigned To User";
                }

        }

        protected void Btndelete_Click(object sender, EventArgs e)
        {
            if (Roles.IsUserInRole(Listboxrole.SelectedItem.Text))
            {

                Roles.RemoveUserFromRole(Listboxuser.SelectedItem.Text,
                Listboxrole.SelectedItem.Text);
                BindUsers();
                BindRoles();
                Label1.Text = "User Removed Successfully";
            }
            else
            {
                Label1.Text = "Error Removing User from Role!!";
            }
        }

        }
    }
