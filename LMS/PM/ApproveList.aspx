﻿<%@ Page Title="Approved List" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="ApproveList.aspx.cs" Inherits="LMS.PM.ApproveList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 202px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="AL">
        <div class="topmarg">
        </div>
        <h4>
            LIST OF ALL APPROVED CANDIDATES</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <asp:UpdatePanel ID="Upangrid" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div>
                    <table class="width80" cellspacing="5">
                        <tr class="center pd_td">
                            <td colspan="4">
                                <h3>
                                    Search Criteria</h3>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Interview List from Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalfrm" runat="server" Enabled="false" ></asp:TextBox>
                                <asp:CalendarExtender ID="tbcal_CalendarExtender" runat="server" Enabled="True" TargetControlID="tbcalfrm"
                                    PopupButtonID="imgcalfrm" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <%--<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="tbcal" runat="server" DaysModeTitleFormat="d MMMM, yyyy" PopupButtonID="imgcal" PopupPosition="TopRight" TodaysDateFormat="d MMMM, yyyy" FirstDayOfWeek="Sunday" Enabled="True"></asp:CalendarExtender>--%>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalfrm" height="25px" />
                            </td>
                            <td>
                                To Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcalto" runat="server"  Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="tbcalto"
                                    PopupButtonID="imgcalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Sunday">
                                </asp:CalendarExtender>
                                <%--<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="tbcal" runat="server" DaysModeTitleFormat="d MMMM, yyyy" PopupButtonID="imgcal" PopupPosition="TopRight" TodaysDateFormat="d MMMM, yyyy" FirstDayOfWeek="Sunday" Enabled="True"></asp:CalendarExtender>--%>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" id="imgcalto" height="25px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Group :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlgrp" runat="server" DataSourceID="SqlDataSource2" DataTextField="ProjName"
                                    DataValueField="ProjName" AppendDataBoundItems="true">
                            <asp:ListItem  Value="---Select---" >---Select---</asp:ListItem>
                            <asp:ListItem  Value="All Groups" >All Groups</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---Select---" ID="RequiredFieldValidator2" ControlToValidate="ddlgrp" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                       
                        <%--        &nbsp;
                                <asp:CheckBox ID="Cbgrp" Text="All Groups" AutoPostBack="true" CssClass="msglabel"
                                    runat="server" OnCheckedChanged="Cbgrp_CheckedChanged" />--%>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                    SelectCommand="GetAllProject_SP" SelectCommandType="StoredProcedure">
                                     <SelectParameters>
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="pname" Type="String" />
                                        <asp:Parameter DefaultValue="&quot;&quot;" Name="type" Type="String" />
                                    </SelectParameters>
                             </asp:SqlDataSource>
                            </td>
                            <td>
                                Venue :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlvenue" runat="server" DataSourceID="SqlDataSource1" 
                                    DataTextField="Venue" DataValueField="Venue" AppendDataBoundItems="true">
                                <asp:ListItem  Value="---Select---" >---Select---</asp:ListItem>
                                <asp:ListItem  Value="All Venues" >All Venues</asp:ListItem>
                                </asp:DropDownList>
                                   <asp:RequiredFieldValidator InitialValue="---Select---" ForeColor="Red" ID="RequiredFieldValidator3" ControlToValidate="ddlvenue" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                       
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                                    SelectCommand="GetAllVenue_SP" SelectCommandType="StoredProcedure">
                                </asp:SqlDataSource>
                              <%--  &nbsp;
                                <asp:CheckBox ID="Cbven" Text="All Venue" AutoPostBack="true" CssClass="msglabel"
                                    runat="server" OnCheckedChanged="Cbven_CheckedChanged" />--%>
                            </td>
                        </tr>
                        <tr class="myhr">
                            <td class="style1">
                            </td>
                        </tr>
                        <tr class="pd_td center">
                            <td colspan="4">
                                <asp:Button ID="Butget" runat="server" Text="RETRIEVE" OnClick="Butget_Click" />
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td class="style1">
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Panel ID="pangrid" runat="server">
                    <div id="gridscroll">
                        <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" 
                            runat="server" onrowdatabound="GridView1_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CbAll" AutoPostBack="true" OnCheckedChanged="SN_ChangedClicked"
                                            Text="SN" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CbRow" AutoPostBack="true" OnCheckedChanged="CbRow_Checked" runat="server" />
                                        <asp:Label ID="lblsn" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                                    NullDisplayText="&quot;No data&quot;">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Identification No." SortExpression="IdNo">
                                    <ItemTemplate>
                                        <asp:Label ID="lblid" runat="server" Text='<%# Bind("ID_Num") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="Qualif" HeaderText="Qualification" SortExpression="Qualif">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Score">
                                    <ItemTemplate>
                                        <asp:Label ID="lblscore" runat="server" Text='<%# Eval("Score") %>'></asp:Label>
                                        <asp:TextBox ID="tbscore" SkinID="GridViewTb" Visible="false" runat="server" MaxLength="2"
                                            Text='<%# Eval("Score") %>'></asp:TextBox>
                                        <%--   <asp:RangeValidator ID="RangeValidator1" runat="server" ForeColor="Red" Font-Size="XX-Small" ControlToValidate="tbscore" SetFocusOnError="true" MaximumValue="100" MinimumValue="0" ErrorMessage="<=100"></asp:RangeValidator>
                                        --%>
                                        <asp:FilteredTextBoxExtender ID="ftbe" runat="server" TargetControlID="tbscore" FilterMode="ValidChars"
                                            FilterType="Numbers">
                                        </asp:FilteredTextBoxExtender>
                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<=99"
                                ValidationExpression="^([0-9]?[0-9])$" ControlToValidate="score" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="55px" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Interview Outcome">
                                    <ItemTemplate>
                                        <asp:Label ID="lbloutcome" runat="server" Text='<%# Eval("Outcome") %>'></asp:Label>
                                        <asp:DropDownList ID="ddloutcome" SkinID="GridDdl" SelectedValue='<%# Eval("Outcome") %>' Visible="false" runat="server">
                                            <asp:ListItem>---SELECT---</asp:ListItem>
                                            <asp:ListItem>Successful</asp:ListItem>
                                            <asp:ListItem>UnSuccessful</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddloutcome"
                                            SetFocusOnError="true" InitialValue="---SELECT---" runat="server" ErrorMessage="*"> </asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reason for Decision">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="170px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblreason" runat="server" Text='<%# Eval("Reason") %>'></asp:Label>
                                        <asp:TextBox ID="tbreason" SkinID="GridTxtArea" Visible="false" MaxLength="100" Rows="2"
                                            Text='<%# Eval("Reason") %>' TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="cbapproveall" AutoPostBack="true" OnCheckedChanged="cbapproveall_checked"
                                            Text="Recommended to Enrol" runat="server" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbapprove" Checked='<%# Convert.ToBoolean(Eval("Recommended")) %>' AutoPostBack="true" OnCheckedChanged="cbapprove_checked"
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="List Approved" >
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="cbapprove" runat="server" />
                        </ItemTemplate>
                        <HeaderTemplate >
                        <asp:CheckBox ID="cbapproveall" runat="server" />
                        </HeaderTemplate>
                    </asp:TemplateField>--%>
                            </Columns>
                            <AlternatingRowStyle BackColor="#EBFDD4" />
                            <RowStyle BackColor="#BEF8CF" />
                            <PagerStyle CssClass="TextboxShadow" />
                        </asp:GridView>
                    </div>
                    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>--%>
                </asp:Panel>
                <div class="clear hrspacer">
                </div>
                <asp:Panel ID="panupdate" runat="server">
                    <div>
                        <div class="clear hrspacer">
                        </div>
                        <div>
                            <table class="width80">
                                <tr align="center">
                                  <td>
                                            <asp:Button ID="butprnt" runat="server" Text="PRINT" />
                                        </td>
                                    <td>
                                        <h4 class="msglabel">
                                            COUNT OF RECOMMENDED =
                                            <asp:Label ID="lblcount" runat="server"></asp:Label>
                                        </h4>
                                    </td>
                                    <td>
                                        <asp:Button ID="butupdat" runat="server" Text="UPDATE" OnClick="butsch_Click2" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="panapprove" runat="server">
                                <table class="width50 center">
                                    <tr>
                                        <td>
                                            <asp:Button ID="butapprove" runat="server" Text="APPROVE LIST" />
                                        </td>
                                      
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
