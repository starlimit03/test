﻿<%@ Page Title="Returned COC and Contracts" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="ContractReturned.aspx.cs" Inherits="LMS.PM.ContractReturned" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="RC">
        <div class="topmarg">
        </div>
        <h4>
            CANDIDATES CONTRACTS & CODE OF CONDUCTS</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <div id="gridscroll">
            <asp:GridView SkinID="Gridall" ID="GridView1" runat="server" DataSourceID="SqlDataSource1"
                Style="margin: auto">
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" InsertVisible="False" ReadOnly="True"
                        SortExpression="SN">
                        <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification Number" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="270px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <%-- <asp:TemplateField HeaderText="Contract Returned?" >
                <ItemStyle HorizontalAlign="Center"  Width="70px" VerticalAlign="Middle" />
            <ItemTemplate>
            <asp:CheckBox ID="cbcontract" runat="server" />
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Code of Conduct Returned?" >
                <ItemStyle HorizontalAlign="Center"  Width="100px" VerticalAlign="Middle" />
            <ItemTemplate>
            <asp:CheckBox ID="cbcoc" runat="server" />
            </ItemTemplate>
            </asp:TemplateField>--%>
                    <asp:TemplateField HeaderText="Interview Outcome">
                        <ItemTemplate>
                            <asp:Label ID="lblconstat" runat="server" Text='<%# Eval("Qualif") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlconstat" runat="server" SkinID="GridDdl">
                                <asp:ListItem>ISSUED</asp:ListItem>
                                <asp:ListItem>SIGNED & RETURNED</asp:ListItem>
                                <asp:ListItem>TERMINATED</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lbldate" runat="server"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="date" Columns="10" SkinID="GridViewTb" runat="server" MaxLength="10"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="dd/mm/20yy"
                                ValidationExpression="^(0?[1-9]|[12][0-9]|3[01])[\/](0?[1-9]|1[012])[\/](20?\d{2})$"
                                ControlToValidate="date" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="80px" VerticalAlign="Middle" />
                    </asp:TemplateField>
                    <asp:CommandField HeaderText="Edit" ShowEditButton="True">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:CommandField>
                </Columns>
                                 <AlternatingRowStyle BackColor="#EBFDD4"  />
                <RowStyle BackColor="#BEF8CF" />
                 <PagerStyle CssClass="TextboxShadow" />
               </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        <div class="clear">
        </div>
        <div>
            <table style="width: 70%; margin: auto;">
                <tr class="pd_td" align="center">
                    <td>
                        <asp:Button ID="butupdate" runat="server" Text="Update List" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
