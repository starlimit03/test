﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer;
using System.Data;

namespace LMS.PM
{
    public partial class ApproveList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                pangrid.Visible = false;
                panupdate.Visible = false;
                tbcalfrm.Text = DateTime.Now.AddMonths(-1).ToShortDateString();
                tbcalto.Text = DateTime.Today.ToShortDateString();
            }
        }

        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
        }

        protected void Butget_Click(object sender, EventArgs e)
        {
            string grp,ven,type;
            Lists lst = new Lists();
                     
          DateTime  frm ,to;// = tbcalto.Text;

         // DateTime.TryParse(tbcalto.Text, out to);
         //   DateTime.TryParse(tbcalfrm.Text, out frm);
          to=  Convert.ToDateTime(tbcalto.Text);
          frm = Convert.ToDateTime(tbcalfrm.Text);

            int chk=frm.CompareTo(to) ;
            if(chk == 1)
            {
                MessageAlert("Error", "Date To cannot preceed date From !!!");
                pangrid.Visible = false; return;
            }

            grp = ddlgrp.SelectedItem.Text;
            ven = ddlvenue.SelectedItem.Text;
            if (grp == "All Groups" && ven == "All Venues")
                type = "allgrp_ven";
            else if (grp == "All Groups" && ven != "All Venues")
                type = "allgrp";
            else if (grp != "All Groups" && ven == "All Venues")
                type = "allvenue";
            else type = "simple";

            DataSet ds = lst.GetScheduledList(frm.ToShortDateString(), to.ToShortDateString(), grp, ven, type);

            if (ds != null)
            {
                if (ds.Tables[0].Columns.Count > 0)
                {
                    // Populate Gridview with Info;
                    GridView1.DataSource = ds;

                    GridView1.DataBind();
                    pangrid.Visible = true;
                    panupdate.Visible = false;
                }
            }
            else
            {
                MessageAlert("ERROR", "No Data for selected Search!!!");
                pangrid.Visible = false;
                panupdate.Visible = false;
            }
        }

        protected void SN_ChangedClicked(object sender, EventArgs e)
        {
             CheckBox cbh = GridView1.HeaderRow.FindControl("CbAll") as CheckBox;
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("CbRow"));
                Label lbsco = ((Label)r.FindControl("lblscore"));
                TextBox tbsco = ((TextBox)r.FindControl("tbscore"));
                Label lboutcome = ((Label)r.FindControl("lbloutcome"));
                DropDownList ddloutcome = ((DropDownList)r.FindControl("ddloutcome"));
                Label lbrsn = ((Label)r.FindControl("lblreason"));
                TextBox tbrsn = ((TextBox)r.FindControl("tbreason"));

                if (cbh.Checked)
                {
                    cbc.Checked = true; lbsco.Visible = false; tbsco.Visible = true; lboutcome.Visible = false;
                    ddloutcome.Visible = true; lbrsn.Visible = false; tbrsn.Visible = true;
                    panupdate.Visible = true;
                }
                else
                {
                    cbc.Checked = false; lbsco.Visible = true; tbsco.Visible = false; lboutcome.Visible = true;
                    ddloutcome.Visible = false; lbrsn.Visible = true; tbrsn.Visible = false;
                }
            }

        }

        protected void CbRow_Checked(object sender, EventArgs e)
        {
          
            CheckBox cb = (CheckBox)sender;
            GridViewRow gvr = (GridViewRow)cb.NamingContainer;

            TextBox tbsco = (TextBox)gvr.FindControl("tbscore");
            Label lbsco = (Label)gvr.FindControl("lblscore");

            DropDownList ddlout = (DropDownList)gvr.FindControl("ddloutcome"); 
            Label lbout = (Label)gvr.FindControl("lbloutcome"); 

            TextBox tbrsn = (TextBox)gvr.FindControl("tbreason");
            Label lbrsn = (Label)gvr.FindControl("lblreason");

            panupdate.Visible = true;
            if (cb.Checked)
            {
                lbsco.Visible = false;
                tbsco.Visible = true;
                ddlout.Visible = true;
                lbout.Visible = false;
                lbrsn.Visible = false;
                tbrsn.Visible = true;
              //   panupdate.Visible = true;
            }
            else
            {
                lbsco.Visible = true;
                tbsco.Visible = false;
                ddlout.Visible = false;
                lbout.Visible = true;
                lbrsn.Visible = true;
                tbrsn.Visible = false;
             //   panupdate.Visible = false;
            }
         
        }

        //protected void butupdat_Click(object sender, EventArgs e)
        //{
        //     int i = 0,score=0; string idnum="", outcome,reason,recommend,type="";
        //    Lists lst = new Lists();
        //    foreach (GridViewRow r in GridView1.Rows)
        //    {
        //        CheckBox cb = ((CheckBox)r.FindControl("CbRow"));
        //        CheckBox cba = ((CheckBox)r.FindControl("cbapprove"));
             
        //       // if (cb.Checked || cba.Checked)
        //        {
        //             score =Convert.ToInt16(((TextBox)r.FindControl("tbscore")).Text);
        //             outcome = ((DropDownList)r.FindControl("ddloutcome")).SelectedItem.Text;
        //             reason = ((TextBox)r.FindControl("tbreason")).Text;
        //             recommend = ((CheckBox)r.FindControl("cbapprove")).Checked.ToString();

        //             idnum = ((Label)r.FindControl("lblid")).Text;
        //             if ((cba.Checked && !cb.Checked) || (!cba.Checked && !cb.Checked))
        //             {
        //                // type = "recommend";
        //                 outcome = ((Label)r.FindControl("lbloutcome")).Text;
        //             }
        //             else 
        //            type = "update";
        //          if (lst.UpdateInterviewList(idnum, score, outcome,reason,recommend,"----",type))
        //              i++;
        //        }
        //    }
        //    if (i > 0)
        //    {
        //        MessageAlert("Info", i + " Rows Successfully Updated!");
               
        //    }
        //    else MessageAlert("Info", "No Row(s) Updated!");

        //    Butget_Click(null, null);
        //    //DataSet ds = lst.GetInterList(idnum, group, status, type);
        //    //if (ds != null)
        //    //{
        //    //    // Populate Gridview with Info;
        //    //    GridView1.DataSource = ds;
        //    //    GridView1.DataBind();
        //    //    pangrid.Visible = true;
        //    //   pansubmit.Visible = false;
        //    //   panprint.Visible = true;
        //    //}
            
        //}
        
        protected void cbapproveall_checked(object sender, EventArgs e)
        {
            panupdate.Visible = true;
           CheckBox cbh = GridView1.HeaderRow.FindControl("cbapproveall") as CheckBox;
           foreach (GridViewRow r in GridView1.Rows)
           {
               CheckBox cbc = ((CheckBox)r.FindControl("cbapprove"));
               CheckBox cbr = ((CheckBox)r.FindControl("CbRow"));
               if (cbh.Checked)
               { cbc.Checked = true; lblcount.Text = GridView1.Rows.Count.ToString();  }//cbr.Checked = true;
               else { cbc.Checked = false;  lblcount.Text = "0"; }
           }
        }

        protected void cbapprove_checked(object sender, EventArgs e)
        {
            panupdate.Visible = true;
            CheckBox cbh = GridView1.HeaderRow.FindControl("cbapproveall") as CheckBox;
            CheckBox cb = (CheckBox)sender;
            int y = 0;
            foreach (GridViewRow r in GridView1.Rows)
            {
                CheckBox cbc = ((CheckBox)r.FindControl("cbapprove"));
                CheckBox cbr = ((CheckBox)r.FindControl("CbRow"));
                if (cbc.Checked)
                {
                    y++;
                   // cbr.Checked = true;
                }

            }
            if (y < GridView1.Rows.Count) cbh.Checked = false;
            //if (y == 0) panupdate.Visible = false;
            lblcount.Text = y.ToString();
        }

        private bool tablecopied = false;
        private DataTable origdatatable;
        private string id,outcome,reason,recommend;
        private int score;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                if (!tablecopied)
                {
                    origdatatable = ((DataRowView)e.Row.DataItem).Row.Table.Copy();
                    ViewState["originalvaluesdatatable"] = origdatatable;
                    tablecopied = true;
                }
        }

        protected void butsch_Click2(object sender, EventArgs e)
        {
            int i = 0;
            origdatatable = (DataTable)ViewState["originalvaluesdatatable"];
            Lists lst = new Lists(); // to update the database based on stored procedures
            foreach(GridViewRow r in GridView1.Rows)
                if (IsModified(r))
                {
                    if (lst.UpdateInterviewList(id,score,outcome,reason,recommend,"----","update"))
                        i++;
                   // GridView1.UpdateRow(r.RowIndex, false);
                }
            if (i > 0)
            {
                MessageAlert("Info", i + " Rows Successfully Updated!");

            }
            else MessageAlert("Info", "No Row(s) Updated!");
            tablecopied = false;
            // GridView1.DataBind();
            //pangrid.Visible = true;

            Butget_Click(null, null);
        }

        protected bool IsModified(GridViewRow r)
        {
         int sco;
         string outcom,outcom2, rsn, recom, idnum;
           // Int32.TryParse(GridView1.DataKeys[r.RowIndex].Value.ToString(), out currentid);
           
           // currentid = GridView1.DataKeys[r.RowIndex].Value.ToString();
           //   currenttime = ((TextBox)r.FindControl("tbtime")).Text;  
           sco =Convert.ToInt16(((TextBox)r.FindControl("tbscore")).Text);
           outcom = ((DropDownList)r.FindControl("ddloutcome")).SelectedItem.Text;
           rsn = ((TextBox)r.FindControl("tbreason")).Text;
           recom = ((CheckBox)r.FindControl("cbapprove")).Checked.ToString();
           outcom2 = ((Label)r.FindControl("lbloutcome")).Text;
            
                CheckBox cb = ((CheckBox)r.FindControl("CbRow"));

           idnum = ((Label)r.FindControl("lblid")).Text;

           id = idnum; score = sco; outcome = outcom2; reason = rsn; recommend = recom;

           DataRow row = origdatatable.Select(string.Format("ID_Num = '{0}'", idnum))[0];

           if (!sco.Equals(Convert.ToInt16(row["Score"]))) { return true; }
           if (!rsn.Equals(row["Reason"].ToString())) { return true; }
           if (cb.Checked) { if (!outcom.Equals(row["Outcome"].ToString())) { outcome = outcom; return true; } }
           else if (!outcom2.Equals(row["Outcome"].ToString())) { outcome = outcom2; return true; }
           if (!recom.Equals(row["Recommended"].ToString())) { return true; }
              return false;
        }

        //protected void checkdates()
        //{
        //     DateTime frm = DateTime.Now,to= DateTime.Now;
        // DateTime.TryParse(tbcalfrm.Text,out frm);
        // DateTime.TryParse(tbcalto.Text,out to);
        // if (frm < to) { MessageAlert("Error", "Date To cannot preceed date From"); }

        //}

     
    }
}