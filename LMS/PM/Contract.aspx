﻿<%@ Page Title="Contract & Code Of Conducts" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Contract.aspx.cs" Inherits="LMS.PM.Contract" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="CC">
        <div class="topmarg">
        </div>
        <h4>
            CANDIDATES CONTRACTS & CODE OF CONDUCTS</h4>
        <hr class="myhr" />
        <div class="hrspacer">
        </div>
        <div id="gridscroll">
             <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                DataSourceID="SqlDataSource1" >
                <Columns>
                    <asp:BoundField DataField="SN" HeaderText="SN" InsertVisible="False" ReadOnly="True"
                        SortExpression="SN">
                        <ItemStyle Width="20px" HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HeaderText="Candidate Name" SortExpression="Name"
                        NullDisplayText="&quot;No data&quot;">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="IdNo" HeaderText="Identification Number" SortExpression="IdNo">
                        <ItemStyle HorizontalAlign="Center" Width="270px" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Contract">
                        <ItemStyle HorizontalAlign="Center" Width="70px" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="cbcontract" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code of Conduct">
                        <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <asp:CheckBox ID="cbcoc" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField HeaderText="Edit" ShowEditButton="True">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    </asp:CommandField>
                </Columns>
                                <AlternatingRowStyle BackColor="#EBFDD4"  />
                <RowStyle BackColor="#BEF8CF" />
                 <PagerStyle CssClass="TextboxShadow" />
               </asp:GridView>
        </div>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
            SelectCommand="SELECT * FROM [testing]"></asp:SqlDataSource>
        <div class="clear"></div>
        <div>
            <table style="width: 70%; margin: auto;">
                <tr class="pd_td">
                    <td>
                        <asp:Button ID="butcontract" runat="server" Text="Generate Contract" />
                    </td>
                    <td>
                        <asp:Button ID="butcoc" runat="server" Text="Generate C.of Conduct " Width="170px"
                            Height="26px" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
