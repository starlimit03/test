﻿<%@ Page Title="Error 404!!!" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master" AutoEventWireup="true" CodeBehind="404Error.aspx.cs" Inherits="LMS._404Error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="topmarg"></div>
  <center>  <h4>
       LEARNER MANAGEMENT SYSTEM
    </h4>
    <h5>404 ERROR!!!</h5>
   <div class=" table-grid-space"></div>
   <div class="center">
  <img id="imgconstruct" alt="under construction"  src="images/background/sorry.png" /> </div>
  <p>The Page you have requested cannot be found!</p>
  <p>Kindly contact the administrators.</p></center>
  <div class="bottommarg"></div>
</asp:Content>
