﻿<%@ Page Title="Program" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Program.aspx.cs" Inherits="LMS.MGT.Program" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 158px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Pgm">
        <h4 class="topmarg">
            PROGRAM CREATION
        </h4>
        <asp:UpdatePanel ID="upall" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:Panel ID="panselect" runat="server">
        <table class="width80 center" cellspacing="5px">
            <tr class="pd_td">
                <td colspan="3">
                    <hr class="hrspacer myhr" />
                </td>
            </tr>
            <tr class="pd_td"><td></td></tr>
            <tr class="pd_td">
                <td>
                    Program Name :
                </td>
                <td>
                    <asp:DropDownList ID="ddlprog" runat="server"  
                        DataTextField="ProgName" DataValueField="ProgName" AppendDataBoundItems="true">
                    <asp:ListItem Value="---Select---">---Select---</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator  ControlToValidate="ddlprog"  InitialValue="---Select---" ID="RequiredFieldValidator1" runat="server" ></asp:RequiredFieldValidator>
                    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                        SelectCommand="GetAllProgram_SP" SelectCommandType="StoredProcedure">
                    </asp:SqlDataSource>--%>
                </td>
                <td>
                 <asp:Button ID="butsearch" runat="server" Text="SEARCH" 
                        onclick="butsearch_Click" />
                </td>
            </tr>
          <tr class="pd_td"><td colspan="2">
          <asp:CheckBox ID="cbnew" Text="New Program" runat="server"  OnCheckedChanged="cbnew_Checked" AutoPostBack="true" />
          </td></tr>
          </table>
       </asp:Panel>
        <asp:Panel ID="panall" Visible="false" runat="server">

          <table class="width80">
            <tr class="pd_td">
                <td colspan="3">
                    <hr class="hrspacer myhr1" />
                </td>
            </tr>
            <tr class="pd_td">
                <td>
                    Program Name :
                </td>
                <td>
              <asp:TextBox ID="tbprognam" runat="server"></asp:TextBox> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" SetFocusOnError="true" runat="server" ControlToValidate="tbprognam" >*</asp:RequiredFieldValidator>   
                          </td>
                <td>
               Description :
                    <asp:TextBox runat="server" ID="tbdesc" SkinID="TextArea" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" SetFocusOnError="true" runat="server" ControlToValidate="tbdesc" >*</asp:RequiredFieldValidator>   
                    </td>
            </tr>
            <tr class="pd_td">
                <td >
               Max No. Of Beneficiaries :
                </td>
                <td>
                 <asp:TextBox ID="tbben" MaxLength="5" runat="server"></asp:TextBox> 
                    <asp:FilteredTextBoxExtender ID="tbben_FilteredTextBoxExtender" runat="server" FilterType="Numbers" FilterMode="ValidChars" 
                        Enabled="True" TargetControlID="tbben">
                    </asp:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" SetFocusOnError="true" ControlToValidate="tbben" runat="server" >*</asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
         
         
            <tr class="pd_td">
                <td colspan="3">
                    <hr class="hrspacer myhr1" />
                </td>
            </tr>
            <tr class="pd_td center"><td colspan="3">
            <asp:Label ID="lblmsg" runat="server"></asp:Label>
            </td></tr>
            <tr align="center" class="pd_td ">
                <td colspan="3">
                    <asp:Button ID="butcreate" runat="server" Text="CREATE" 
                        onclick="butcreate_Click" />
                    <asp:ConfirmButtonExtender ID="butcreate_ConfirmButtonExtender" runat="server" 
                        ConfirmText="Confirm CREATE/UPDATE of Program ?" Enabled="True" TargetControlID="butcreate">
                    </asp:ConfirmButtonExtender>
                </td>
            </tr>
        </table>
        </asp:Panel>
        </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
