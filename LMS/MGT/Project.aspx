﻿<%@ Page Title="Project" Theme="MyTheme" Language="C#" MasterPageFile="~/LMS.Master"
    AutoEventWireup="true" CodeBehind="Project.aspx.cs" Inherits="LMS.MGT.Project" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PD">
        <h4 class="topmarg">
            PROJECT CREATION
        </h4>
        <asp:UpdatePanel runat="server" ID="upall">
            <ContentTemplate>
                <table style="height: auto; margin: auto" class=" width90" cellspacing="6px">
                    <tr class="pd_td">
                        <td colspan="3">
                            <hr class="hrspacer myhr" />
                        </td>
                    </tr>
                    <tr class="pd_td">
                        <td>
                        </td>
                    </tr>
                    <tr class="pd_td">
                        <td>
                            Program Name :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlprogram" runat="server" DataSourceID="SqlDataSource1" DataTextField="ProgName"
                                DataValueField="ProgName" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlprogram_SelectedIndexChanged">
                                <asp:ListItem Value="---SELECT---">---SELECT---</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ddlprogram" ID="RequiredFieldValidator1"
                                runat="server" ErrorMessage="*" InitialValue=">---SELECT---">
                            </asp:RequiredFieldValidator>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                SelectCommand="GetAllProgram_SP" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbutlist" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                Width="305px" OnSelectedIndexChanged="rbutlist_SelectedIndexChanged">
                                <asp:ListItem> New Project </asp:ListItem>
                                <asp:ListItem> Update Project </asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr class="pd_td">
                        <td>
                            Max No. of Beneficiary :
                        </td>
                        <td>
                            <asp:Label ID="labben" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td>
                            Remaining No. of Beneficiary :
                            <asp:Label ID="labrben" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr class="pd_td">
                        <td colspan="3">
                            <div id="divprj" runat="server" visible="false">
                                <table class=" width50" id="tbprjs" runat="server">
                                    <tr class="pd_td">
                                        <td>
                                            Projects :
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlproj" runat="server" AppendDataBoundItems="True" AutoPostBack="True"
                                                OnSelectedIndexChanged="ddlproj_SelectedIndexChanged">
                                                <asp:ListItem Value="---SELECT---">---SELECT---</asp:ListItem>
                                            </asp:DropDownList>
                                            <%--   <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                            ConnectionString="<%$ ConnectionStrings:testingConnectionString %>" 
                                            SelectCommand="SELECT [ProjName] FROM [Project_T] WHERE ([ProgID] = @ProgID)">
                              <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:testingConnectionString %>"
                                SelectCommand="GetAllProject_SP" SelectCommandType="StoredProcedure">            
                                          
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="ddlprogram" DefaultValue="test" Name="ProgID" 
                                                    PropertyName="SelectedValue" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>--%>
                                            <asp:RequiredFieldValidator ControlToValidate="ddlproj" ID="RequiredFieldValidator10"
                                                runat="server" ErrorMessage="*" InitialValue=">---SELECT---">
                                            </asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="pd_td">
                        <td colspan="3">
                            <hr class="hrspacer myhr1" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="panall" Visible="false" runat="server">
                    <table class="width90" cellspacing="5">
                        <tr class="pd_td">
                            <td colspan="4">
                                <h3 class="center">
                                    Project Information</h3>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Name :
                            </td>
                            <td>
                                <asp:TextBox ID="tbpname" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="tbpname" ID="RequiredFieldValidator7"
                                    runat="server" ErrorMessage="*">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td style="vertical-align: middle;">
                                Description :
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="tbdesc" SkinID="TextArea" TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ControlToValidate="tbdesc" ID="RequiredFieldValidator8"
                                    runat="server" ErrorMessage="*">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Start Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbstart" runat="server" Enabled="false" ViewStateMode="Enabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ControlToValidate="tbstart"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" class="imgcal" id="imgcalfrm" />
                                <%--   <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="MM/DD/YYYY"
                        Operator="DataTypeCheck" Type="Date" ControlToValidate="tbstart" Font-Size="XX-Small"
                        ForeColor="Red" SetFocusOnError="true"></asp:CompareValidator>
                    &nbsp;--%>
                                <asp:CalendarExtender ID="tbcal_CalendarExtender" runat="server" Enabled="True" TargetControlID="tbstart"
                                    PopupButtonID="imgcalfrm" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                            </td>
                            <td>
                                End Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbend" runat="server" Enabled="false" EnableViewState="true"></asp:TextBox>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" class="imgcal" id="imgcalto" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ControlToValidate="tbend"
                                    runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <%--  <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="MM/DD/YYYY"
                        Operator="DataTypeCheck" Font-Size="XX-Small" ForeColor="Red" SetFocusOnError="true"
                        Type="Date" ControlToValidate="tbend"></asp:CompareValidator>--%>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="tbend"
                                    PopupButtonID="imgcalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Phase :
                            </td>
                            <td>
                                <asp:TextBox ID="tbphase" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Beneficiaries
                            </td>
                            <td>
                                <asp:TextBox ID="tbbenef" MaxLength="5" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" Enabled="True"
                                    TargetControlID="tbbenef" FilterMode="ValidChars" ValidChars="0123456789">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ControlToValidate="tbbenef" ID="RequiredFieldValidator16"
                                    runat="server" ErrorMessage="*">
                                </asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Sponsor Name :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlsponsor" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlsponsor_SelectedIndexChanged">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>MICT SETA</asp:ListItem>
                                    <asp:ListItem>Rockerfeller</asp:ListItem>
                                    <asp:ListItem>ABSA</asp:ListItem>
                                    <asp:ListItem>Engen</asp:ListItem>
                                    <asp:ListItem>RWF</asp:ListItem>
                                    <asp:ListItem>Accenture</asp:ListItem>
                                    <asp:ListItem>BriteHouse</asp:ListItem>
                                    <asp:ListItem>Microsoft</asp:ListItem>
                                    <asp:ListItem>--OTHERS--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="ddlsponsor" ID="RequiredFieldValidator2"
                                    runat="server" ErrorMessage="*" InitialValue="---SELECT---">
                                </asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divothers" visible="false">
                                <td>
                                    Others :
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="tbothers"></asp:TextBox>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Sponsor Group:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlgrp" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlgrp_SelectedIndexChanged">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>Global</asp:ListItem>
                                    <asp:ListItem>Local</asp:ListItem>
                                    <asp:ListItem>--OTHERS--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ControlToValidate="ddlgrp" ID="RequiredFieldValidator3"
                                    runat="server" ErrorMessage="*" InitialValue="---SELECT---">
                                </asp:RequiredFieldValidator>
                            </td>
                            <div runat="server" id="divothergrp" visible="false">
                                <td>
                                    Others :
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="tbothergrp"></asp:TextBox>
                                </td>
                            </div>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Eligibility :
                            </td>
                            <td colspan="3">
                                <asp:CheckBoxList ID="cblprogelig" runat="server" RepeatDirection="Horizontal" CellSpacing="5"
                                    Width="100%">
                                    <asp:ListItem>Masters</asp:ListItem>
                                    <asp:ListItem>Honors</asp:ListItem>
                                    <asp:ListItem>N.Diploma</asp:ListItem>
                                    <asp:ListItem>Certificate</asp:ListItem>
                                    <asp:ListItem>College</asp:ListItem>
                                    <asp:ListItem>Matric</asp:ListItem>
                                    <asp:ListItem>None</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Credits :
                            </td>
                            <td>
                                <asp:TextBox ID="tbcrdt" MaxLength="3" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbcrdt_FilteredTextBoxExtender" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="tbcrdt">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <td>
                                NQF Level :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlnqfl" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>BELOW 4</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>--NONE--</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator5"
                                    ControlToValidate="ddlnqfl" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Stipend Allocation :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlstip" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstip_SelectedIndexChanged">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>YES</asp:ListItem>
                                    <asp:ListItem>NO</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator4"
                                    ControlToValidate="ddlstip" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                Unit Standards :
                            </td>
                            <td>
                                <asp:TextBox ID="tbunitstd" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <asp:Panel ID="panstip" Visible="false" runat="server">
                                    <table cellspacing="10" width="100%">
                                        <tr class="pd_td">
                                            <td>
                                                Stipend Amount (R) :
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbamt" MaxLength="7" runat="server"></asp:TextBox>
                                                <asp:FilteredTextBoxExtender ID="tbamt_FilteredTextBoxExtender" runat="server" Enabled="True"
                                                    TargetControlID="tbamt" FilterMode="ValidChars" FilterType="Numbers">
                                                </asp:FilteredTextBoxExtender>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Font-Size="XX-Small"
                                                    runat="server" ForeColor="Red" ErrorMessage="**" ValidationExpression="^([0-9]*)$"
                                                    ControlToValidate="tbamt" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Font-Size="XX-Small"
                                                    runat="server" ForeColor="Red" ErrorMessage="**" ValidationExpression="^([0-9]*)([0-9][0-9])$"
                                                    ControlToValidate="tbamt" SetFocusOnError="True"></asp:RegularExpressionValidator>--%>
                                            </td>
                                            <td>
                                                Stipend Frequency :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlfreq" runat="server">
                                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                                    <asp:ListItem>WEEKLY</asp:ListItem>
                                                    <asp:ListItem>MONTHLY</asp:ListItem>
                                                    <asp:ListItem>PROGRAM</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator6"
                                                    ControlToValidate="ddlfreq" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr class="pd_td">
                                            <td>
                                                Deductions :
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddldeduct" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddldeduct_SelectedIndexChanged">
                                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                                    <asp:ListItem>YES</asp:ListItem>
                                                    <asp:ListItem>NO</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator11"
                                                    ControlToValidate="ddldeduct" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                            </td>
                                            <div id="divdeduct" visible="false" runat="server">
                                                <td>
                                                    Deduction Amount (R) :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbdeduct" MaxLength="5" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator12"
                                                        ControlToValidate="tbdeduct" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" Enabled="True"
                                                        TargetControlID="tbdeduct" FilterMode="ValidChars" FilterType="Numbers">
                                                    </asp:FilteredTextBoxExtender>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Font-Size="XX-Small"
                                                        runat="server" ForeColor="Red" ErrorMessage="**" ValidationExpression="^([0-9]*)$"
                                                        ControlToValidate="tbdeduct" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                                </td>
                                            </div>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="hrspacer myhr1" />
                            </td>
                        </tr>
                        <tr align="center" class="pd_td ">
                            <td colspan="4">
                                <asp:Button ID="butcreate" runat="server" Text="CREATE PROJECT" OnClick="butcreate_Click" />
                                <asp:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="True"
                                    ConfirmText="Confirm CREATE/UPDATE of Project ?" TargetControlID="butcreate">
                                </asp:ConfirmButtonExtender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pansite" Visible="false">
                    <table cellspacing="5" class="width90">
                        <tr class="pd_td">
                            <td colspan="4">
                                <h3 class="center">
                                    Site Information</h3>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Site Name :
                            </td>
                            <td>
                                <asp:TextBox ID="tbsitename" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Site Province :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlprovince" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>National</asp:ListItem>
                                    <asp:ListItem>Gauteng</asp:ListItem>
                                    <asp:ListItem>Mpumalanga</asp:ListItem>
                                    <asp:ListItem>Limpopo</asp:ListItem>
                                    <asp:ListItem>Western Cape</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator13"
                                    ControlToValidate="ddlprovince" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Site Region :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlregion" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>--Test---</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator14"
                                    ControlToValidate="ddlregion" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                                Site Town :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddltown" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>--Test---</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" InitialValue="---SELECT---" ID="RequiredFieldValidator15"
                                    ControlToValidate="ddltown" SetFocusOnError="true" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Primary Beneficiary
                            </td>
                            <td>
                                <asp:TextBox ID="tbpbenef" MaxLength="5" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbbenef_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" TargetControlID="tbbenef" FilterMode="ValidChars" 
                                    FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ControlToValidate="tbpbenef" ID="RequiredFieldValidator9"
                                    runat="server" ErrorMessage="*">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td>
                                Secondary Beneficiary
                            </td>
                            <td>
                                <asp:TextBox ID="tbsbenef" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbsbenef_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" TargetControlID="tbsbenef">
                                </asp:FilteredTextBoxExtender>
                            </td>
                            <%-- <td>
                                Total Estimated Budget (R) : </td><td>
                                <asp:TextBox ID="tbprogbud" runat="server"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="tbprogbud_FilteredTextBoxExtender" runat="server"
                                    Enabled="True" TargetControlID="tbprogbud" FilterMode="ValidChars" ValidChars="0123456789.">
                                </asp:FilteredTextBoxExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Font-Size="XX-Small"
                                    runat="server" ForeColor="Red" ErrorMessage="**" ValidationExpression="^([0-9]*)$"
                                    ControlToValidate="tbprogbud" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </td>--%>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Start Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbssdate" runat="server" Enabled="false" ViewStateMode="Enabled"></asp:TextBox>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" class="imgcal" id="imgscalfrm" />
                                <%--   <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="MM/DD/YYYY"
                        Operator="DataTypeCheck" Type="Date" ControlToValidate="tbstart" Font-Size="XX-Small"
                        ForeColor="Red" SetFocusOnError="true"></asp:CompareValidator>
                    &nbsp;--%>
                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="tbssdate"
                                    PopupButtonID="imgscalfrm" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                            </td>
                            <td>
                                End Date :
                            </td>
                            <td>
                                <asp:TextBox ID="tbsedate" runat="server" Enabled="false" EnableViewState="true"></asp:TextBox>
                                &nbsp;<img alt="calendar" src="../images/Styling/calendar.jpg" class="imgcal" id="imgscalto" />
                                <%--  <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="MM/DD/YYYY"
                        Operator="DataTypeCheck" Font-Size="XX-Small" ForeColor="Red" SetFocusOnError="true"
                        Type="Date" ControlToValidate="tbend"></asp:CompareValidator>--%>
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="tbsedate"
                                    PopupButtonID="imgscalto" PopupPosition="TopRight" Format="MM/dd/yyyy" FirstDayOfWeek="Monday">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <h5 class="center">
                                    Site Contact Details</h5>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Contact Name :
                            </td>
                            <td>
                                <asp:TextBox ID="tbsitecname" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                Contact Number :
                            </td>
                            <td>
                                <asp:TextBox ID="tbscno" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <h5 class="center">
                                    Site Responsibilities</h5>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Manager :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlpm" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>Linda</asp:ListItem>
                                    <asp:ListItem>Sarah</asp:ListItem>
                                    <asp:ListItem>Chris</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Technical Manager :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddltm" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>Linda</asp:ListItem>
                                    <asp:ListItem>Sarah</asp:ListItem>
                                    <asp:ListItem>Chris</asp:ListItem>
                                    <asp:ListItem>Patrick</asp:ListItem>
                                    <asp:ListItem>Nobert</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td>
                                Project Administrator :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlpa" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>Linda</asp:ListItem>
                                    <asp:ListItem>Sarah</asp:ListItem>
                                    <asp:ListItem>Chris</asp:ListItem>
                                    <asp:ListItem>Patrick</asp:ListItem>
                                    <asp:ListItem>Nobert</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                Project Facilitator :
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlpfac" runat="server">
                                    <asp:ListItem>---SELECT---</asp:ListItem>
                                    <asp:ListItem>Linda</asp:ListItem>
                                    <asp:ListItem>Sarah</asp:ListItem>
                                    <asp:ListItem>Patrick</asp:ListItem>
                                    <asp:ListItem>Nobert</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                                <hr class="hrspacer myhr" />
                            </td>
                        </tr>
                        <tr align="center" class="pd_td ">
                            <td colspan="4">
                                <asp:Button ID="butsite" runat="server" Text="ADD SITE" OnClick="butsite_Click" />
                                <asp:ConfirmButtonExtender ID="butsite_ConfirmButtonExtender" runat="server" Enabled="True"
                                    ConfirmText="Confirm ADD/UPDATE of Project Site ?" TargetControlID="butsite">
                                </asp:ConfirmButtonExtender>
                            </td>
                        </tr>
                        <tr class="pd_td">
                            <td colspan="4">
                            </td>
                        </tr>
                    </table>
                    <%--<asp:Panel runat="server" ID="pangrid" Visible="true">
                        <div id="gridscroll">
                            <asp:GridView SkinID="Gridall" ID="GridView1" Style="margin: auto" runat="server"
                                OnRowDataBound="GridView1_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="SN">
                                    
                                        <ItemTemplate>
                                          
                                            <asp:Label ID="lblsn" runat="server" Text='<%# Container.DataItemIndex +1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SiteName" HeaderText="Project Site" NullDisplayText="&quot;No data&quot;">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblid" runat="server" Text='<%# Bind("SiteId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project Site">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsite" runat="server" Text='<%# Bind("SiteName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Province">
                                        <ItemTemplate>
                                            <asp:Label ID="lblprovince" runat="server" Text='<%# Bind("Province") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Beneficiaries">
                                        <ItemTemplate>
                                            <asp:Label ID="lblbenef" runat="server" Text='<%# Eval("PBenef") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="55px" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Facilitator">
                                        <ItemTemplate>
                                            <asp:Label ID="lblreason" runat="server" Text='<%# Eval("P_Fac") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="100px" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Start-Date" />
                                    <asp:BoundField HeaderText="End-Date" />
                                </Columns>
                                <AlternatingRowStyle BackColor="#EBFDD4" />
                                <RowStyle BackColor="#BEF8CF" />
                                <PagerStyle CssClass="TextboxShadow" />
                            </asp:GridView>
                        </div>
                    </asp:Panel>--%>
                </asp:Panel>
                <%-- </td></tr>--%>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="bottommarg">
        </div>
    </div>
</asp:Content>
