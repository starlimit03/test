﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer;

namespace LMS.MGT
{
    public partial class Project : System.Web.UI.Page
    {
        static string SelectedProjName=""
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!Page.IsPostBack)
            {
                tbothers.Enabled = false;
                tbothergrp.Enabled = false;
                rbutlist.SelectedIndex = -1;
                rbutlist.SelectedIndex = 0;
            }
        }

        protected void ddlsponsor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsponsor.SelectedItem.ToString() == "--OTHERS--")
                divothers.Visible = true;
            else
                divothers.Visible = false;
        }

        protected void ddlgrp_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlgrp.SelectedItem.ToString() == "--OTHERS--")
                divothergrp.Visible = true;
            else
                divothergrp.Visible = false;
        }

        protected void ddlstip_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlstip.SelectedItem.ToString() == "YES")
                panstip.Visible = true;
            else panstip.Visible = false;
        }

        protected void butcreate_Click(object sender, EventArgs e)
        {
            if (cblprogelig.SelectedIndex < 0) 
            { MessageAlert("Error!", "Check Project Eligibility!!!"); return; }

            //if (ddlprovince.SelectedIndex < 0) 
            //{ MessageAlert("Error!", "Check Project Eligibility!!!"); return; }

            ProjectInfo proj = new ProjectInfo();

            string pgname = ddlprogram.SelectedItem.ToString();
            string pjname = tbpname.Text;
            string desc = tbdesc.Text;
            string sponsnam = "",sponsgrp="";
           
            if (ddlsponsor.SelectedItem.ToString() == "--OTHERS--")
                sponsnam=tbothers.Text;
            else
                 sponsnam = ddlsponsor.SelectedItem.ToString();
          
            if (ddlgrp.SelectedItem.ToString() == "--OTHERS--")
                sponsgrp = tbothergrp.Text;
            else
                sponsgrp = ddlgrp.SelectedItem.ToString();
          
            string sdat =  tbstart.Text;
            string edat = tbend.Text;  //CalendarExtender2.SelectedDate.ToString();// 
            string elig = "";
          
            ListItemCollection listitem = cblprogelig.Items;
              foreach(ListItem  li in listitem)
              {
                  if (li.Selected) elig += li.Text+",";
                 
              }

            elig=  elig.Substring(0, elig.Length - 1);
           
            string phase = tbphase.Text;
         //   string ploc = "";   //lbloc.SelectedItem.ToString();


            //double bud  Convert.ToDouble(tbprogbud.Text);
            int ben =Convert.ToInt16( tbbenef.Text);
            string ustd = tbunitstd.Text;
            int crdt =  Convert.ToInt16(tbcrdt.Text);
            string nqf = ddlnqfl.SelectedItem.ToString();
            string stipend = ddlstip.SelectedItem.ToString();
            
            double stpamt=0,deduction=0;string stifrq="";
           string deduct="";
            if (stipend == "YES")
            {
                 stpamt = Convert.ToDouble(tbamt.Text);
                 stifrq= ddlfreq.SelectedItem.ToString();
                 if (ddldeduct.SelectedItem.Text == "YES")
                 {deduct="YES"; deduction = Convert.ToDouble(tbdeduct.Text);}
            }

            //ListItemCollection listitems = ddlprovince.Items;
            //int count = 0;
           
            //foreach (ListItem li in listitems)
            //{
            // // if (li.Selected) ploc += li.Text + ",";
            //    if (li.Selected)
            //    {
            //        ploc = li.Text;
            //        // create individual projects based on the project locations.
            //        if (proj.CreateProject(pjname, pgname, desc, sdat, edat, sponsnam, sponsgrp,
            //        ben, elig, ustd, crdt, nqf, stipend, stpamt, stifrq, phase, deduct, deduction))
            //            count = 0;
            //        else
            //            count++;
            //    }
            //}
            //ploc = ploc.Substring(0, ploc.Length - 1);  // delect the last occurence of ','

            if (proj.CreateProject(pjname, pgname, desc, sdat, edat, sponsnam, sponsgrp,
                  ben, elig, ustd, crdt, nqf, stipend, stpamt, stifrq, phase, deduct, deduction))
           
          // if (count == 0)
            {
                panstip.Visible = false;
                MessageAlert("Success!!!", "Project Successfully Created !!!");
                panall.Enabled = false;
                pansite.Visible = true;
                ddlprogram.Enabled = false;
                SelectedProjName = ddlproj.SelectedItem.Text;
               // Clear();
            }
            else
            {
           //     ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ERROR!!!", "alert('" + "Error creating Project !!!" + "');", true);

                MessageAlert("ERROR!!!", "Error creating Project !!!");  //  lblmsg.Text = "Error creating Program!!!";
            }
        }

        private void Clear()
        {
            Panel uppanel = Master.FindControl("ContentPlaceHolder1").FindControl("upall").FindControl("panall") as Panel;

            foreach (var txbox in uppanel.Controls)
            {
                if (txbox is TextBox)
                    ((TextBox)txbox).Text = "";

            }

           foreach (var txbox in uppanel.Controls)
            {
                if (txbox is DropDownList)
                {
                    if (((DropDownList)txbox).DataSource == null)
                        ((DropDownList)txbox).Text = "---SELECT---";
                }

            }
        }
        private void MessageAlert(string alert, string msg)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), alert, "alert('" + msg + "');", true);
         
        }

        protected void ddldeduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddldeduct.SelectedItem.Text == "YES")
                divdeduct.Visible = true;
            else divdeduct.Visible = false;
        }

        protected void ddlprogram_SelectedIndexChanged(object sender, EventArgs e)
        {

          //  panall.Visible = false;
           
            if (ddlprogram.SelectedIndex == 0)
            {
                labben.ForeColor = Color.Green;
                labben.Text = "0";
                labrben.Text = "0";
                panall.Visible = false; 
                divprj.Visible = false;
                pansite.Visible = false;
                return;
            }
            

            if (rbutlist.SelectedIndex == 1)
            {
                divprj.Visible = true;

            }
            else
            {
                panall.Visible = true; 
                divprj.Visible = false;
                              
            }
           
            // get Max number of beneficiary and Sum of Beneficiary for the selected Program
            labben.Text = "0"; labrben.Text = "0";
            ProjectInfo prj = new ProjectInfo();
            if (prj.GetBeneficiary(ddlprogram.SelectedItem.Text))
            {

                labben.ForeColor = Color.Green;
                labben.Text = prj.Benef.ToString();
                labrben.Text = (prj.Benef - prj.SumBenef).ToString();
                // Populate all Projects related to the selected Program

                DataSet ds = prj.GetAllProject(ddlprogram.SelectedItem.Text,"ByProg"); 
                //  da.Fill(ds, "Roles");

                if (ds != null)
                {
                    ddlproj.Items.Clear();

                    //ddlproj.DataSource = new List<string>();
                    //ddlproj.DataValueField = "ProjName";
                    //ddlproj.DataBind();
                    

                    ddlproj.DataSource = ds;
                     DataRow dr =ds.Tables[0].NewRow();
                    dr["ProjName"]="---SELECT---";
                    ds.Tables[0].Rows.InsertAt(dr,0);
                    
                    ddlproj.DataTextField = "ProjName";
                    ddlproj.DataValueField= "ProjName";
                    ddlproj.DataBind();

                }
            }
            else
            {
                labben.ForeColor = Color.Red;
                labben.Text = prj.Benef.ToString(); ;
                labrben.Text = "No Data!";
            }
        }

        protected void rbutlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbutlist.SelectedIndex == 0)
            {
                ddlprogram.Enabled = true;
                if (ddlprogram.SelectedIndex != 0)
                {
                    divprj.Visible = false;
                    panall.Visible = true;
                   // ddlprogram.Enabled = false;
                    butcreate.Text = "CREATE";
                }
                else
                {
                    divprj.Visible = false;
                    panall.Visible = false;
                }
               
            }
            else
            {
                if (ddlprogram.SelectedIndex != 0)
                {
                    divprj.Visible = true;
                    panall.Visible = false;
                    butcreate.Text = "UPDATE";
                    //ddlprogram.Enabled = false;
                }
            }
        }

        protected void ddlproj_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlproj.SelectedIndex != 0)
            {
                // get Project Information....
            
                panall.Visible = true;
            }
            else panall.Visible = false;
        }

        protected void butsite_Click(object sender, EventArgs e)
        {
            string pname = ddlproj.SelectedItem.Text,
                            sname=tbsitename.Text, 
                            sdate = tbssdate.Text,
                            edate = tbsedate.Text,
                            province = ddlprovince.SelectedItem.Text,
                            region = ddlregion.SelectedItem.Text,
                            town = ddltown.SelectedItem.Text,
                            p_mgr = ddlpm.SelectedItem.Text,
                            t_mgr = ddltm.SelectedItem.Text,
                            p_admin = ddlpa.SelectedItem.Text,
                            p_fac = ddlpfac.SelectedItem.Text;
            int pbenef = Convert.ToInt16(tbpbenef.Text), sbenef=Convert.ToInt16(tbsbenef.Text);


            ProjectInfo pinf = new ProjectInfo();
            if (pinf.CreateSite(pname, sname, pbenef, sbenef, sdate, edate,
           province, region, town, p_mgr, t_mgr, p_admin, p_fac))

           MessageAlert("Success!!!", "Project Site Successfully Created !!!");
       else
           MessageAlert("Error!!!", "Error Creating Project Site !!!");
             


        }

      

    }
}