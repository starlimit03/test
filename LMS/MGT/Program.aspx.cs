﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogicLayer;
using System.Data;
using LMS;


namespace LMS.MGT
{
    public partial class Program : System.Web.UI.Page
    {
      
        
      private  ProgInfo prog = new ProgInfo();

    
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            lblmsg.Text = "";
            if (!Page.IsPostBack)
                Bind();

        }

        private void Bind()
        {
            // Rebind DDL of All Programs
            DataSet ds = prog.GetAllProgram();  //new DataSet();
            //  da.Fill(ds, "Roles");
            if (ds != null)
            {
                ddlprog.DataSource = ds;
                ddlprog.DataTextField = "ProgName";
                ddlprog.DataValueField = "ProgName";
                ddlprog.DataBind();
            }
        }


        protected void butcreate_Click(object sender, EventArgs e)
        {
           
          string nam=  tbprognam.Text;
          string desc = tbdesc.Text;
          int benef = Convert.ToInt16(tbben.Text);
          if (prog.CreateProgram(nam, desc, benef))
          {
             //  lblmsg.Text = "Program Successfully Created!!!";
              Bind();
              Clear();
              MessageAlert( "Success!!!", "Program Successfully "+butcreate.Text+"D!!!" );
              panall.Visible = false;
          }
          else
          {
              MessageAlert( "ERROR!!!",  "Error creating Program!!!!!!" );

            //  lblmsg.Text = "Error creating Program!!!";
          } 
                     

        }
        protected void MessageAlert(string title, string body)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), title, "alert('" + body + "');", true);
           
        }
        protected void butsearch_Click(object sender, EventArgs e)
        {
            if (prog.GetProgramInfo(ddlprog.SelectedItem.ToString()))
            {
                tbprognam.Text = prog.ProgName;
                tbdesc.Text = prog.Desc;
                tbben.Text = prog.Benef.ToString();
                butcreate.Text = "UPDATE";
                panall.Visible = true;
            }
            else
                lblmsg.Text ="Error Retrieving Program Information!!!";
        }

        protected void cbnew_Checked(object sender, EventArgs e)
        {
            ddlprog.SelectedIndex = 1;
            if (cbnew.Checked==true) { Clear(); ddlprog.Enabled = false; butsearch.Enabled = false; butcreate.Text = "CREATE"; panall.Visible = true; }
            else { butsearch.Enabled = true; panall.Visible = false; ddlprog.Enabled = true; }
        }

        private void Clear()
        {
            Panel uppanel = Master.FindControl("ContentPlaceHolder1").FindControl("upall").FindControl("panall") as Panel;

            foreach (var txbox in uppanel.Controls)
            {
                if (txbox is TextBox)
                    ((TextBox)txbox).Text = "";

            }
            //foreach (var txbox in uppanel.Controls)
            //{
            //    if (txbox is DropDownList)
            //    {
            //        if (((DropDownList)txbox).DataSource == null)
            //            ((DropDownList)txbox).Text = "---Select---";
            //    }

            //}
        }
    }
}