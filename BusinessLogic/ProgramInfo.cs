﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccessLayer;
using LMS;

namespace BusinessLogic
{
    public class ProgramInfo
    {
        public int Progid { get; set; }
        public string ProgName { get; set; }
        public string Desc { get; set; }
        public int Benef { get; set; }


        public bool CreateProgram(string name, string desc, int benef)
        {
            // Change to id to use Session ID;
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();

                // remove ................Pick from xml
                objPar.Add(new csParameterListType("pname", SqlDbType.VarChar, name));
                objPar.Add(new csParameterListType("pdesc", SqlDbType.VarChar, desc));
                objPar.Add(new csParameterListType("pbenef", SqlDbType.Int, benef));
                if (objDAL.executespreturnnd("CreateProgram_SP", objPar))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;

            }

        }

        public bool GetProgramInfo(string name)
        {
            try
            {
                csDAL objDAL = new csDAL();
                List<csParameterListType> objPar = new List<csParameterListType>();

                // remove ................Pick from xml
                objPar.Add(new csParameterListType("pname", SqlDbType.VarChar, name));

                using (IDataReader dr = objDAL.executespreturndr("GetProgramInfo_SP", objPar))
               {
                  if (dr.Read())
                  {
                      ProgName = dr.GetValue(1).ToString();
                      Desc= dr.GetValue(2).ToString();
                      Benef =Convert.ToInt16(dr.GetValue(3));
                  }
                  return true;
              }
            }
            catch (Exception ex)
            {
                return false;

            }

        }

        public DataSet GetAllProgram()
        {
            // Change to id to use Session ID;
            try
            {
                csDAL objDAL = new csDAL();
                DataSet ds = new DataSet();
                ds = objDAL.executespreturnds("GetAllProgram_SP");
                if(ds!=null)
                    return ds;
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;

            }

        }
    }
}

